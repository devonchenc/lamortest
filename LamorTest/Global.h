#pragma once

#define INSB_RSV                    0x00
#define INSB_READ_DEVICE_INFO       0x01
#define INSB_CHANGE_BAUDRATE        0x02
#define INSB_COM_TEST               0x03
#define INSB_READ_STATUS            0x04
#define INSB_CLEAR_ALL_ERROR        0x05
#define INSB_SYNC_TIME              0x06
#define INSB_READ_TIME              0x07
#define INSB_WRITE_PARA             0x08
#define INSB_READ_PARA              0x09
#define INSB_START_ACQUIRE          0x0A
#define INSB_READ_ACQUIRED_DATA     0x0B
#define INSB_WRITE_RAW_DATA         0x0C
#define INSB_READ_RAW_DATA          0x0D
#define INSB_ENTER_DVP_MODE         0x0E
#define INSB_EXIT_DVP_MODE          0x0F
#define INSB_TEMINATE_EXE           0x10

#define INSB_ENTER_BSL_MODE         0x28

#define INSB_READ_AUX_INFO          0x40

enum InfoType
{
    DeviceType = 1,
    HardwareVersion,
    SoftwareVersion,
    RAMVersion,
};

enum ParaType
{
    FIR = 1,
    CIC,
    TIMECORE,
};

enum TimeCoreType
{
    TXS = 1,
    WAIT = 2,
    LOOPS = 3,
    LOOPE = 4,
    RXE = 5,
    OTL = 6,
    RXLP = 7,
    END = 15
};

#include <string>

// 获取当前时间字符串
std::string getDataTime();

// 获取时间戳字符串
std::string getDataTime2(int seconds);

// 向日志发送开始
void logStart(const std::string& operation);