#pragma once

#include <QDialog>

QT_BEGIN_NAMESPACE
class QGridLayout;
QT_END_NAMESPACE

class WriteRamDialog : public QDialog
{
    Q_OBJECT

public:
    WriteRamDialog(QWidget* parent = nullptr);

signals:
    void writeRam(int ramAddress, std::vector<int> data);

private:
    void initUI();

private slots:
    void addButtonClicked();

    void writeButtonClicked();

private:
    QGridLayout* _gridLayout;
};
