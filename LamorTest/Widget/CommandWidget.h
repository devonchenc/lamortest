#pragma once

#include <QWidget>

QT_BEGIN_NAMESPACE
class QPushButton;
class QDomElement;
QT_END_NAMESPACE

class WriteRamDialog;
class ReadRamDialog;

class CommandWidget : public QWidget
{
    Q_OBJECT

public:
    CommandWidget(QWidget* parent = nullptr);

public slots:
    void onWriteRam(int ramAddress, std::vector<int> data);

    void onReadRam(int ramAddress, int readLen);

private slots:
    void comTestButtonClicked();

    void readTimeButtonClicked();

    void syncTimeButtonClicked();

    void enterDVPButtonClicked();

    void exitDVPButtonClicked();

    void enterBSLButtonClicked();

    void writeRAMButtonClicked();

    void readRAMButtonClicked();

    void writeParaButtonClicked();

    void readInfoButtonClicked();

    void startAcqButtonClicked();

    void terminateExeButtonClicked();

    void readStatusButtonClicked();

    void readAuxInfoButtonClicked();

    void changeBaudrateButtonClicked();

    void runXMLButtonClicked();

signals:
    void saveXmlFile();

private:
    void initUI();

    // ����xml�ļ�
    void parseXmlFile(const QString& fileName, const QStringList& filter = QStringList());

    void parseInstruction(const QDomElement& element);

    void parseTimeCoreInstruction(const QDomElement& element);

private:
    WriteRamDialog* _writeRamdlg;
    ReadRamDialog* _readRamdlg;

    QPushButton* _comTestButton;
    QPushButton* _readTimeButton;
    QPushButton* _syncTimeButton;
    QPushButton* _enterDVPButton;
    QPushButton* _exitDVPButton;
    QPushButton* _enterBSLButton;
    QPushButton* _writeRAMButton;
    QPushButton* _readRAMButton;
    QPushButton* _writeParaButton;
    QPushButton* _readInfoButton;
    QPushButton* _startAcqButton;
    QPushButton* _terminateExeButton;
    QPushButton* _readStatusButton;
    QPushButton* _readAuxInfoButton;
    QPushButton* _changeBaudrateButton;
    QPushButton* _runXMLButton;
};
