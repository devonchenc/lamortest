#include "WriteRamDialog.h"

#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include <QGroupBox>
#include <QHBoxLayout>

const int LINE_NUM = 8;

WriteRamDialog::WriteRamDialog(QWidget* parent)
    : QDialog(parent)
{
    setWindowTitle(tr("Write RAM"));

    initUI();
}

void WriteRamDialog::initUI()
{
    _gridLayout = new QGridLayout;
    _gridLayout->addWidget(new QLabel("Address:"), 0, 1);
    QSpinBox* addressSpinBox = new QSpinBox;
    addressSpinBox->setFixedWidth(90);
    addressSpinBox->setMaximum(INT_MAX);

    _gridLayout->addWidget(addressSpinBox, 1, 1);
    _gridLayout->addWidget(new QLabel("Data:"), 2, 1);

    for (int i = 0; i < LINE_NUM; i++)
    {
        if (i > 0)
        {
            QPushButton* addButton = new QPushButton("+");
            addButton->setFixedWidth(30);
            connect(addButton, &QPushButton::clicked, this, &WriteRamDialog::addButtonClicked);
            _gridLayout->addWidget(addButton, i + 3, 0);
        }

        QSpinBox* dataSpinBox = new QSpinBox;
        dataSpinBox->setFixedWidth(90);
        dataSpinBox->setMaximum(INT_MAX);
        if (i > 0)
        {
            dataSpinBox->setEnabled(false);
        }
        _gridLayout->addWidget(dataSpinBox, i + 3, 1);
    }

    QGroupBox* addressGroupBox = new QGroupBox;
    addressGroupBox->setLayout(_gridLayout);

    QPushButton* writeButton = new QPushButton(tr("Write"));
    connect(writeButton, &QPushButton::clicked, this, &WriteRamDialog::writeButtonClicked);
    QPushButton* rejectButton = new QPushButton(tr("Cancel"));
    connect(rejectButton, &QPushButton::clicked, this, &QDialog::reject);
    QHBoxLayout* hLayout = new QHBoxLayout;
    hLayout->addStretch();
    hLayout->addWidget(writeButton);
    hLayout->addStretch();
    hLayout->addWidget(rejectButton);
    hLayout->addStretch();

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(addressGroupBox);
    mainLayout->addLayout(hLayout);
    setLayout(mainLayout);
}

void WriteRamDialog::addButtonClicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    int index = _gridLayout->indexOf(button);
    QSpinBox* dataSpinBox = qobject_cast<QSpinBox*>(_gridLayout->itemAt(index + 1)->widget());

    if (button->text() == "+")
    {
        button->setText("-");
        dataSpinBox->setEnabled(true);
    }
    else
    {
        button->setText("+");
        dataSpinBox->setEnabled(false);
    }
}

void WriteRamDialog::writeButtonClicked()
{
    QSpinBox* addressSpinBox = qobject_cast<QSpinBox*>(_gridLayout->itemAtPosition(1, 1)->widget());
    int address = addressSpinBox->value();

    std::vector<int> data;
    for (int i = 0; i < LINE_NUM; i++)
    {
        QSpinBox* dataSpinBox = qobject_cast<QSpinBox*>(_gridLayout->itemAtPosition(i + 3, 1)->widget());
        if (dataSpinBox->isEnabled())
        {
            data.push_back(dataSpinBox->value());
        }
    }

    emit writeRam(address, data);
}
