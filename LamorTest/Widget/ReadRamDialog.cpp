#include "ReadRamDialog.h"

#include <QLabel>
#include <QSpinBox>
#include <QPushButton>
#include <QGroupBox>
#include <QHBoxLayout>

const int LINE_NUM = 8;

ReadRamDialog::ReadRamDialog(QWidget* parent)
    : QDialog(parent)
{
    setWindowTitle(tr("Read RAM"));

    initUI();
}

void ReadRamDialog::initUI()
{
    _gridLayout = new QGridLayout;
    _gridLayout->addWidget(new QLabel("Address:"), 0, 1);
    _gridLayout->addWidget(new QLabel("Length:"), 0, 2);

    for (int i = 0; i < LINE_NUM; i++)
    {
        if (i > 0)
        {
            QPushButton* addButton = new QPushButton("+");
            addButton->setFixedWidth(30);
            connect(addButton, &QPushButton::clicked, this, &ReadRamDialog::addButtonClicked);
            _gridLayout->addWidget(addButton, i + 1, 0);
        }

        QSpinBox* addressSpinBox = new QSpinBox;
        QSpinBox* lengthSpinBox = new QSpinBox;
        addressSpinBox->setFixedWidth(90);
        addressSpinBox->setMaximum(INT_MAX);
        lengthSpinBox->setFixedWidth(90);
        lengthSpinBox->setMaximum(INT_MAX);
        if (i > 0)
        {
            addressSpinBox->setEnabled(false);
            lengthSpinBox->setEnabled(false);
        }
        _gridLayout->addWidget(addressSpinBox, i + 1, 1);
        _gridLayout->addWidget(lengthSpinBox, i + 1, 2);
    }

    QGroupBox* addressGroupBox = new QGroupBox;
    addressGroupBox->setLayout(_gridLayout);

    QPushButton* readButton = new QPushButton(tr("Read"));
    connect(readButton, &QPushButton::clicked, this, &ReadRamDialog::readButtonClicked);
    QPushButton* rejectButton = new QPushButton(tr("Cancel"));
    connect(rejectButton, &QPushButton::clicked, this, &QDialog::reject);
    QHBoxLayout* hLayout = new QHBoxLayout;
    hLayout->addStretch();
    hLayout->addWidget(readButton);
    hLayout->addStretch();
    hLayout->addWidget(rejectButton);
    hLayout->addStretch();

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(addressGroupBox);
    mainLayout->addLayout(hLayout);
    setLayout(mainLayout);
}

void ReadRamDialog::addButtonClicked()
{
    QPushButton* button = qobject_cast<QPushButton*>(sender());
    int index = _gridLayout->indexOf(button);
    QSpinBox* addressSpinBox = qobject_cast<QSpinBox*>(_gridLayout->itemAt(index + 1)->widget());
    QSpinBox* lengthSpinBox = qobject_cast<QSpinBox*>(_gridLayout->itemAt(index + 2)->widget());

    if (button->text() == "+")
    {
        button->setText("-");
        addressSpinBox->setEnabled(true);
        lengthSpinBox->setEnabled(true);
    }
    else
    {
        button->setText("+");
        addressSpinBox->setEnabled(false);
        lengthSpinBox->setEnabled(false);
    }
}

void ReadRamDialog::readButtonClicked()
{
    for (int i = 0; i < LINE_NUM; i++)
    {
        QSpinBox* addressSpinBox = qobject_cast<QSpinBox*>(_gridLayout->itemAtPosition(i + 1, 1)->widget());
        QSpinBox* lengthSpinBox = qobject_cast<QSpinBox*>(_gridLayout->itemAtPosition(i + 1, 2)->widget());

        if (addressSpinBox->isEnabled())
        {
            int address = addressSpinBox->value();
            int length = lengthSpinBox->value();

            emit readRam(address, length);
        }
    }
}
