#pragma once

#include <QDialog>

QT_BEGIN_NAMESPACE
class QGridLayout;
QT_END_NAMESPACE

class ReadRamDialog : public QDialog
{
    Q_OBJECT

public:
    ReadRamDialog(QWidget* parent = nullptr);

signals:
    void readRam(int ramAddress, int readLen);

private:
    void initUI();

private slots:
    void addButtonClicked();

    void readButtonClicked();

private:
    QGridLayout* _gridLayout;
};
