#pragma once

#include <QWidget>

QT_BEGIN_NAMESPACE
class QGroupBox;
class QTextEdit;
class QTextBrowser;
QT_END_NAMESPACE

class InfoWidget : public QWidget
{
    Q_OBJECT

public:
    InfoWidget(QWidget* parent = nullptr);

public:
    void openFile(const QString& fileName);

    void saveFile(const QString& fileName);

    void onShowInfo(const std::string& outputText);

    void onShowTitle(const std::string& outputText);

    void onShowWarning(const std::string& outputText);

    void onShowError(const std::string& outputText);
    void onShowErrorWithCode(int errorCode, const std::string& outputText);

public slots:
    void saveXmlFile();

private:
    void initUI();

private slots:
    void showMenu(const QPoint& pos);

private:
    QGroupBox* _commandGroupBox;
    QTextEdit* _commandEdit;
    QTextBrowser* _infoEdit;
};
