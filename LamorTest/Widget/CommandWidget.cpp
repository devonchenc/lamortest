#include "CommandWidget.h"

#include <QPushButton>
#include <QVBoxLayout>
#include <QInputDialog>
#include <QMessageBox>
#include <QSettings>
#include <QCoreApplication>
#include <QFile>
#include <QDomDocument>

#include "../Actions/ActionRunner.h"
#include "../Actions/CommonAction.h"
#include "../Actions/BoardAction.h"
#include "../Dumper.h"
#include "WriteRamDialog.h"
#include "ReadRamDialog.h"

CommandWidget::CommandWidget(QWidget* parent)
    : QWidget(parent)
    , _writeRamdlg(nullptr)
    , _readRamdlg(nullptr)
{
    initUI();
}

void CommandWidget::initUI()
{
    _comTestButton = new QPushButton(tr("COM Test"));
    _readTimeButton = new QPushButton(tr("Read Time"));
    _syncTimeButton = new QPushButton(tr("Sync Time"));
    _enterDVPButton = new QPushButton(tr("Enter DVP"));
    _exitDVPButton = new QPushButton(tr("Exit DVP"));
    _enterBSLButton = new QPushButton(tr("Enter BSL"));
    _writeRAMButton = new QPushButton(tr("Write RAM"));
    _readRAMButton = new QPushButton(tr("Read RAM"));
    _writeParaButton = new QPushButton(tr("Write Para"));
    _readInfoButton = new QPushButton(tr("Read Info"));
    _startAcqButton = new QPushButton(tr("Start Acq"));
    _terminateExeButton = new QPushButton(tr("Terminate Exe"));
    _readStatusButton = new QPushButton(tr("Read Status"));
    _readAuxInfoButton = new QPushButton(tr("Read Aux Info"));
    _changeBaudrateButton = new QPushButton(tr("Change Bandrate"));
    _runXMLButton = new QPushButton(tr("Run XML"));

    _comTestButton->setFixedSize(110, 25);
    _readTimeButton->setFixedSize(110, 25);
    _syncTimeButton->setFixedSize(110, 25);
    _enterDVPButton->setFixedSize(110, 25);
    _exitDVPButton->setFixedSize(110, 25);
    _enterBSLButton->setFixedSize(110, 25);
    _writeRAMButton->setFixedSize(110, 25);
    _readRAMButton->setFixedSize(110, 25);
    _writeParaButton->setFixedSize(110, 25);
    _readInfoButton->setFixedSize(110, 25);
    _startAcqButton->setFixedSize(110, 25);
    _terminateExeButton->setFixedSize(110, 25);
    _readStatusButton->setFixedSize(110, 25);
    _readAuxInfoButton->setFixedSize(110, 25);
    _changeBaudrateButton->setFixedSize(110, 25);
    _runXMLButton->setFixedSize(110, 25);

    QVBoxLayout* layout = new QVBoxLayout;
    layout->addWidget(_comTestButton);
    layout->addWidget(_readTimeButton);
    layout->addWidget(_syncTimeButton);
    layout->addWidget(_enterDVPButton);
    layout->addWidget(_exitDVPButton);
    layout->addWidget(_enterBSLButton);
    layout->addWidget(_writeRAMButton);
    layout->addWidget(_readRAMButton);
    layout->addWidget(_writeParaButton);
    layout->addWidget(_readInfoButton);
    layout->addWidget(_startAcqButton);
    layout->addWidget(_terminateExeButton);
    layout->addWidget(_readStatusButton);
    layout->addWidget(_readAuxInfoButton);
    layout->addWidget(_changeBaudrateButton);
    layout->addWidget(_runXMLButton);

    connect(_comTestButton, &QPushButton::clicked, this, &CommandWidget::comTestButtonClicked);
    connect(_readTimeButton, &QPushButton::clicked, this, &CommandWidget::readTimeButtonClicked);
    connect(_syncTimeButton, &QPushButton::clicked, this, &CommandWidget::syncTimeButtonClicked);
    connect(_enterDVPButton, &QPushButton::clicked, this, &CommandWidget::enterDVPButtonClicked);
    connect(_exitDVPButton, &QPushButton::clicked, this, &CommandWidget::exitDVPButtonClicked);
    connect(_enterBSLButton, &QPushButton::clicked, this, &CommandWidget::enterBSLButtonClicked);
    connect(_writeRAMButton, &QPushButton::clicked, this, &CommandWidget::writeRAMButtonClicked);
    connect(_readRAMButton, &QPushButton::clicked, this, &CommandWidget::readRAMButtonClicked);
    connect(_writeParaButton, &QPushButton::clicked, this, &CommandWidget::writeParaButtonClicked);
    connect(_readInfoButton, &QPushButton::clicked, this, &CommandWidget::readInfoButtonClicked);
    connect(_startAcqButton, &QPushButton::clicked, this, &CommandWidget::startAcqButtonClicked);
    connect(_terminateExeButton, &QPushButton::clicked, this, &CommandWidget::terminateExeButtonClicked);
    connect(_readStatusButton, &QPushButton::clicked, this, &CommandWidget::readStatusButtonClicked);
    connect(_readAuxInfoButton, &QPushButton::clicked, this, &CommandWidget::readAuxInfoButtonClicked);
    connect(_changeBaudrateButton, &QPushButton::clicked, this, &CommandWidget::changeBaudrateButtonClicked);
    connect(_runXMLButton, &QPushButton::clicked, this, &CommandWidget::runXMLButtonClicked);

    setLayout(layout);
}

void CommandWidget::comTestButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new ComTestAction));
}

void CommandWidget::readTimeButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new ReadTimeAction));
}

void CommandWidget::syncTimeButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new SyncTimeAction));
}

void CommandWidget::enterDVPButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new EnterDVPModeAction));
}

void CommandWidget::exitDVPButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new ExitDVPModeAction));
}

void CommandWidget::enterBSLButtonClicked()
{
    QMessageBox::StandardButton button = QMessageBox::question(this, "LamorTest", tr("Are you sure you want to enter BSL mode?"));
    if (button == QMessageBox::Yes)
    {
        ActionRunner::instance().runAction(ActionPtr(new EnterBSLModeAction));
    }
}

void CommandWidget::writeRAMButtonClicked()
{
    if (_writeRamdlg == nullptr)
    {
        _writeRamdlg = new WriteRamDialog(this);
        connect(_writeRamdlg, &WriteRamDialog::writeRam, this, &CommandWidget::onWriteRam);
    }

    _writeRamdlg->show();
}

void CommandWidget::onWriteRam(int ramAddress, std::vector<int> data)
{
    ActionRunner::instance().runAction(ActionPtr(new WriteRamDataAction(ramAddress, data)));
}

void CommandWidget::readRAMButtonClicked()
{
    if (_readRamdlg == nullptr)
    {
        _readRamdlg = new ReadRamDialog(this);
        connect(_readRamdlg, &ReadRamDialog::readRam, this, &CommandWidget::onReadRam);
    }

    _readRamdlg->show();
}

void CommandWidget::onReadRam(int ramAddress, int readLen)
{
    ActionRunner::instance().runAction(ActionPtr(new ReadRamDataAction(ramAddress, readLen)));
}

void CommandWidget::writeParaButtonClicked()
{
    emit saveXmlFile();

    // 解析xml文件
    QStringList filter;
    filter.append("WriteFIRParameter");
    filter.append("WriteCICParameter");
    filter.append("WriteTimeCoreParameter");
    parseXmlFile(QCoreApplication::applicationDirPath() + "/temp.xml", filter);
}

void CommandWidget::readInfoButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::DeviceType)));
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::HardwareVersion)));
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::SoftwareVersion)));
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::RAMVersion)));
}

void CommandWidget::readStatusButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new ReadStatusAction));
}

void CommandWidget::terminateExeButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new TerminateExeAction));
}

void CommandWidget::startAcqButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new StartAcquireAction));
}

void CommandWidget::readAuxInfoButtonClicked()
{
    ActionRunner::instance().runAction(ActionPtr(new ReadAuxInfoAction));
}

void CommandWidget::changeBaudrateButtonClicked()
{
    QSettings settings(QCoreApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    QString comPort = settings.value("COM/port", "COM1").toString();
    int baudrate = settings.value("COM/baudrate", 1000000).toInt();

    bool ok;
    int value = QInputDialog::getInt(this, tr("Input Baudrate"), tr("Please Input Baudrate:"), baudrate, 9600, 1000000, 100, &ok);
    if (ok)
    {
        ActionRunner::instance().runAction(ActionPtr(new ChangeBaudrateAction(value)));
    }
}

void CommandWidget::runXMLButtonClicked()
{
    emit saveXmlFile();

    // 解析xml文件
    parseXmlFile(QCoreApplication::applicationDirPath() + "/temp.xml");
}

void CommandWidget::parseXmlFile(const QString& fileName, const QStringList& filter)
{
    QFile file(fileName);
    if (!file.open(QIODevice::ReadOnly))
    {
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read instruction file error")));
        return;
    }

    QDomDocument dom("Instructions");
    if (!dom.setContent(&file))
    {
        file.close();
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Parse instruction file error")));
        return;
    }
    file.close();

    QDomElement docElem = dom.documentElement();
    QDomNode node = docElem.firstChild();
    while (!node.isNull())
    {
        QDomElement element = node.toElement(); // try to convert the node to an element.
        if (!element.isNull())
        {
            if (!filter.isEmpty())
            {
                if (filter.contains(element.tagName()))
                {
                    parseInstruction(element);
                }
            }
            else
            {
                parseInstruction(element);
            }
        }
        node = node.nextSibling();
    }
}

void CommandWidget::parseInstruction(const QDomElement& element)
{
    QString tagName = element.tagName();
    if (tagName == "ReadDeviceInfo")
    {
        InfoType infoType = InfoType(element.attribute("type", "0").toInt());
        ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(infoType)));
    }
    else if (tagName == "ComTest")
    {
        ActionRunner::instance().runAction(ActionPtr(new ComTestAction));
    }
	else if (tagName == "ChangeBaudrate")
	{
		int baudrate = element.attribute("baudrate", "1000000").toInt();
		ActionRunner::instance().runAction(ActionPtr(new ChangeBaudrateAction(baudrate)));
	}
    else if (tagName == "ReadStatusWord")
    {
        ActionRunner::instance().runAction(ActionPtr(new ReadStatusAction));
    }
	else if (tagName == "ClearAllError")
	{
		ActionRunner::instance().runAction(ActionPtr(new ClearAllErrorAction));
	}
    else if (tagName == "SyncTime")
    {
        ActionRunner::instance().runAction(ActionPtr(new SyncTimeAction));
    }
    else if (tagName == "ReadTime")
    {
        ActionRunner::instance().runAction(ActionPtr(new ReadTimeAction));
    }
    else if (tagName == "WriteFIRParameter")
    {
        QString str = element.attribute("FIR", "0");
        QStringList paraList = str.split(",");
        std::vector<int> value;
        for (int i = 0; i < paraList.size(); i++)
        {
            value.push_back(paraList.at(i).toInt());
        }
        ActionRunner::instance().runAction(ActionPtr(new WriteFIRParameterAction(value)));
    }
    else if (tagName == "WriteCICParameter")
    {
        int value = element.attribute("CIC", "0").toInt();
        ActionRunner::instance().runAction(ActionPtr(new WriteCICParameterAction(value)));
    }
    else if (tagName == "WriteTimeCoreParameter")
    {
        parseTimeCoreInstruction(element);
    }
    else if (tagName == "ReadParameter")
    {
        ActionRunner::instance().runAction(ActionPtr(new ReadParameterAction));
    }
    else if (tagName == "StartAcquire")
    {
        ActionRunner::instance().runAction(ActionPtr(new StartAcquireAction));
    }
    else if (tagName == "ReadAcquiredData")
    {
        ActionRunner::instance().runAction(ActionPtr(new ReadAcquiredDataAction));
    }
    else if (tagName == "WriteRamData")
    {
        int ramAddress = element.attribute("address", "0").toInt();
        QString str = element.attribute("data", "0");
        QStringList dataList = str.split(",");
        std::vector<int> data;
        for (int i = 0; i < dataList.size(); i++)
        {
            data.push_back(dataList.at(i).toInt());
        }
        ActionRunner::instance().runAction(ActionPtr(new WriteRamDataAction(ramAddress, data)));
    }
    else if (tagName == "ReadRamData")
    {
        int ramAddress = element.attribute("address", "0").toInt();
        int readLen = element.attribute("read_data_length", "1").toInt();
        ActionRunner::instance().runAction(ActionPtr(new ReadRamDataAction(ramAddress, readLen)));
    }
    else if (tagName == "EnterDVPMode")
    {
        ActionRunner::instance().runAction(ActionPtr(new EnterDVPModeAction));
    }
    else if (tagName == "ExitDVPMode")
    {
        ActionRunner::instance().runAction(ActionPtr(new ExitDVPModeAction));
    }
    else if (tagName == "TerminateExe")
    {
        ActionRunner::instance().runAction(ActionPtr(new TerminateExeAction));
    }
    else if (tagName == "ReadAuxInfo")
    {
        ActionRunner::instance().runAction(ActionPtr(new ReadAuxInfoAction));
    }
    else if (tagName == "EnterBSLMode")
    {
        ActionRunner::instance().runAction(ActionPtr(new EnterBSLModeAction));
    }
}

void CommandWidget::parseTimeCoreInstruction(const QDomElement& element)
{
    int address = element.attribute("address", "0").toInt();

    std::vector<std::map<std::string, std::string>> timecore;

    QDomNode node = element.firstChild();
    while (!node.isNull())
    {
        QDomElement childElement = node.toElement(); // try to convert the node to an element.
        if (!childElement.isNull())
        {
            QString name = childElement.tagName();
            if (name == "address")
            {
                address = childElement.text().toInt();
            }
            else if (name == "TXS")
            {
                std::string dacen = childElement.attribute("dacen", "2").toStdString();
                std::string tx_phase = childElement.attribute("tx_phase", "0").toStdString();
                std::string frequency = childElement.attribute("frequency", "15").toStdString();
                std::string dac_ratio = childElement.attribute("dac_ratio", "1").toStdString();
                std::string load = childElement.attribute("load", "0").toStdString();

                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "TXS"));
                parameters.insert(std::pair<std::string, std::string>("dacen", dacen));
                parameters.insert(std::pair<std::string, std::string>("tx_phase", tx_phase));
                parameters.insert(std::pair<std::string, std::string>("frequency", frequency));
                parameters.insert(std::pair<std::string, std::string>("dac_ratio", dac_ratio));
                parameters.insert(std::pair<std::string, std::string>("load", load));
                timecore.push_back(parameters);
            }
            else if (name == "WAIT")
            {
                std::string dataT = childElement.attribute("dataT", "0").toStdString();

                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "WAIT"));
                parameters.insert(std::pair<std::string, std::string>("dataT", dataT));
                timecore.push_back(parameters);
            }
            else if (name == "LOOPS")
            {
                std::string rloopsel = childElement.attribute("Rloopsel", "0").toStdString();
                std::string dataR = childElement.attribute("dataR", "0").toStdString();

                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "LOOPS"));
                parameters.insert(std::pair<std::string, std::string>("Rloopsel", rloopsel));
                parameters.insert(std::pair<std::string, std::string>("dataR", dataR));
                timecore.push_back(parameters);
            }
            else if (name == "LOOPE")
            {
                std::string rloopsel = childElement.attribute("Rloopsel", "0").toStdString();
                std::string addra = childElement.attribute("addra", "0").toStdString();

                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "LOOPE"));
                parameters.insert(std::pair<std::string, std::string>("Rloopsel", rloopsel));
                parameters.insert(std::pair<std::string, std::string>("addra", addra));
                timecore.push_back(parameters);
            }
            else if (name == "RXE")
            {
                std::string rxen0 = childElement.attribute("rxen0", "0").toStdString();
                std::string rxen1 = childElement.attribute("rxen1", "0").toStdString();
                std::string rxen2 = childElement.attribute("rxen2", "0").toStdString();
                std::string rxen3 = childElement.attribute("rxen3", "0").toStdString();
                std::string rxen4 = childElement.attribute("rxen4", "0").toStdString();
                std::string rxen5 = childElement.attribute("rxen5", "0").toStdString();

                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "RXE"));
                parameters.insert(std::pair<std::string, std::string>("rxen0", rxen0));
                parameters.insert(std::pair<std::string, std::string>("rxen1", rxen1));
                parameters.insert(std::pair<std::string, std::string>("rxen2", rxen2));
                parameters.insert(std::pair<std::string, std::string>("rxen3", rxen3));
                parameters.insert(std::pair<std::string, std::string>("rxen4", rxen4));
                parameters.insert(std::pair<std::string, std::string>("rxen5", rxen5));
                timecore.push_back(parameters);
            }
            else if (name == "OTL")
            {
                std::string pamp_ctr = childElement.attribute("pamp_ctr", "0").toStdString();
                std::string outer_ctrl = childElement.attribute("outer_ctrl", "0").toStdString();

                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "OTL"));
                parameters.insert(std::pair<std::string, std::string>("pamp_ctr", pamp_ctr));
                parameters.insert(std::pair<std::string, std::string>("outer_ctrl", outer_ctrl));
                timecore.push_back(parameters);
            }
            else if (name == "RXLP")
            {
                std::string load = childElement.attribute("load", "0").toStdString();
                std::string frequency = childElement.attribute("frequency", "0").toStdString();
                std::string rx_phase = childElement.attribute("rx_phase", "0").toStdString();
                std::string acq_number = childElement.attribute("acq_number", "0").toStdString();

                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "RXLP"));
                parameters.insert(std::pair<std::string, std::string>("load", load));
                parameters.insert(std::pair<std::string, std::string>("frequency", frequency));
                parameters.insert(std::pair<std::string, std::string>("rx_phase", rx_phase));
                parameters.insert(std::pair<std::string, std::string>("acq_number", acq_number));
                timecore.push_back(parameters);
            }
            else if (name == "END")
            {
                std::map<std::string, std::string> parameters;
                parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "END"));
                timecore.push_back(parameters);
            }
        }
        node = node.nextSibling();
    }

    if (timecore.size() > 0)
    {
        ActionRunner::instance().runAction(ActionPtr(new WriteTimeCoreParameterAction(address, timecore)));
    }
}