#pragma once

#include <QDialog>

QT_BEGIN_NAMESPACE
class QComboBox;
class QLineEdit;
QT_END_NAMESPACE

class SettingsDialog : public QDialog
{
    Q_OBJECT

public:
    SettingsDialog(QWidget* parent = nullptr);

private:
    void initUI();

    void listComPorts();

    void loadSettings();

private slots:
    void acceptButtonClicked();

private:
    QComboBox* _comComboBox;
    QLineEdit* _baudrateLineEdit;
};
