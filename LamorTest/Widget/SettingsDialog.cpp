#include "SettingsDialog.h"

#include <QLabel>
#include <QComboBox>
#include <QLineEdit>
#include <QPushButton>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QtSerialPort/QSerialPort>
#include <QtSerialPort/QSerialPortInfo>
#include <QSettings>
#include <QCoreApplication>

const int LINE_NUM = 8;

SettingsDialog::SettingsDialog(QWidget* parent)
    : QDialog(parent)
{
    setWindowTitle(tr("Settings"));

    initUI();

    listComPorts();

    resize(300, 120);

    loadSettings();
}

void SettingsDialog::initUI()
{
    _comComboBox = new QComboBox;
    _baudrateLineEdit = new QLineEdit;
    _baudrateLineEdit->setMaximumWidth(70);
    _baudrateLineEdit->setValidator(new QIntValidator(_baudrateLineEdit));

    QHBoxLayout* hLayout1 = new QHBoxLayout;
    hLayout1->addWidget(new QLabel("COM:"));
    hLayout1->addWidget(_comComboBox);
    hLayout1->addStretch(0);
    hLayout1->addWidget(new QLabel(tr("Baudrate:")));
    hLayout1->addWidget(_baudrateLineEdit);
    hLayout1->addWidget(new QLabel(tr("bps")));

    QGroupBox* comGroupBox = new QGroupBox;
    comGroupBox->setLayout(hLayout1);

    QPushButton* writeButton = new QPushButton(tr("Accept"));
    connect(writeButton, &QPushButton::clicked, this, &SettingsDialog::acceptButtonClicked);
    QPushButton* rejectButton = new QPushButton(tr("Cancel"));
    connect(rejectButton, &QPushButton::clicked, this, &QDialog::reject);
    QHBoxLayout* hLayout2 = new QHBoxLayout;
    hLayout2->addStretch();
    hLayout2->addWidget(writeButton);
    hLayout2->addStretch();
    hLayout2->addWidget(rejectButton);
    hLayout2->addStretch();

    QVBoxLayout* mainLayout = new QVBoxLayout;
    mainLayout->addWidget(comGroupBox);
    mainLayout->addLayout(hLayout2);
    setLayout(mainLayout);
}

void SettingsDialog::listComPorts()
{
    foreach (const QSerialPortInfo& info, QSerialPortInfo::availablePorts())
    {
        _comComboBox->addItem(info.portName());
    }
}

void SettingsDialog::loadSettings()
{
    QSettings settings(QCoreApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    QString comPort = settings.value("COM/port", "COM1").toString();
    int baudrate = settings.value("COM/baudrate", 1000000).toInt();

    _comComboBox->setCurrentText(comPort);
    _baudrateLineEdit->setText(QString::number(baudrate));
}

void SettingsDialog::acceptButtonClicked()
{
    QSettings settings(QCoreApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    settings.setValue("COM/port", _comComboBox->currentText());
    settings.setValue("COM/baudrate", _baudrateLineEdit->text().toInt());

    accept();
}