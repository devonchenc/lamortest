#include "InfoWidget.h"

#include <QTextEdit>
#include <QTextBrowser>
#include <QGroupBox>
#include <QHBoxLayout>
#include <QMenu>
#include <QFile>
#include <QTextStream>
#include <QCoreApplication>

#include "../Thirdparty/XmlHighlighter.h"

InfoWidget::InfoWidget(QWidget* parent)
    : QWidget(parent)
{
    initUI();

    openFile(QCoreApplication::applicationDirPath() + "/temp.xml");
}

void InfoWidget::initUI()
{
    _commandEdit = new QTextEdit;
    _commandEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _commandEdit->setTabStopWidth(fontMetrics().width(' ') * 4);
    new XmlHighlighter(_commandEdit->document());

    QVBoxLayout* vLayout1 = new QVBoxLayout;
    vLayout1->addWidget(_commandEdit);

    _infoEdit = new QTextBrowser;
    _infoEdit->setReadOnly(true);
    _infoEdit->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOn);
    _infoEdit->setStyleSheet("QTextEdit{background-color:rgba(232,235,237,255);}");
    _infoEdit->setContextMenuPolicy(Qt::CustomContextMenu);
    connect(_infoEdit, &QTextBrowser::customContextMenuRequested, this, &InfoWidget::showMenu);
    QVBoxLayout* vLayout2 = new QVBoxLayout;
    vLayout2->addWidget(_infoEdit);

    _commandGroupBox = new QGroupBox(tr("Command"));
    _commandGroupBox->setLayout(vLayout1);
    QGroupBox* infoGroupBox = new QGroupBox(tr("Info"));
    infoGroupBox->setLayout(vLayout2);

    QHBoxLayout* layout = new QHBoxLayout;
    layout->addWidget(_commandGroupBox);
    layout->addWidget(infoGroupBox);

    setLayout(layout);
}

void InfoWidget::openFile(const QString& fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::ReadOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        QString str = stream.readAll();
        _commandEdit->setText(str);
        file.close();

        int index = fileName.lastIndexOf("/");
        _commandGroupBox->setTitle("Command - " + fileName.mid(index + 1));
    }
}

void InfoWidget::saveFile(const QString& fileName)
{
    QFile file(fileName);
    if (file.open(QIODevice::WriteOnly | QIODevice::Text))
    {
        QTextStream stream(&file);
        stream << _commandEdit->toPlainText();
        file.close();
    }
}

void InfoWidget::onShowInfo(const std::string& outputText)
{
    _infoEdit->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
    _infoEdit->insertHtml(QString::fromStdString(outputText + "<br>"));
}

void InfoWidget::onShowWarning(const std::string& outputText)
{
    _infoEdit->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
    _infoEdit->insertHtml(QString::fromStdString("<font color='darkorange'>" + outputText + "<font><br>"));
}

void InfoWidget::onShowError(const std::string& outputText)
{
    _infoEdit->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
    _infoEdit->insertHtml(QString::fromStdString("<font color='red'>" + outputText + "<font><br>"));
}

void InfoWidget::onShowErrorWithCode(int errorCode, const std::string& outputText)
{
    _infoEdit->moveCursor(QTextCursor::End, QTextCursor::MoveAnchor);
    QString str = QString("ErrorCode:%1, %2").arg(errorCode).arg(QString::fromStdString(outputText));
    _infoEdit->insertHtml("<font color='red'>" + str + "<font><br>");
}

void InfoWidget::saveXmlFile()
{
    saveFile(QCoreApplication::applicationDirPath() + "/temp.xml");
}

void InfoWidget::showMenu(const QPoint& pos)
{
    QMenu* menu = _infoEdit->createStandardContextMenu();
    menu->move(_infoEdit->mapToGlobal(pos));
    menu->addSeparator();
    QAction* clearAction = menu->addAction(tr("Clear"));
    connect(clearAction, &QAction::triggered, _infoEdit, &QTextBrowser::clear);
    menu->exec();
}