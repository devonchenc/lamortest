#pragma once

#include <functional>

#include "Operations/SafeQueue.h"

typedef std::function<void(const std::string&)> DumpHandler;

class Dumper
{
public:
    static Dumper& instance();

    void info(const char* format, ...);
    void info(const std::string& str);

    void warning(const char* format, ...);
    void warning(const std::string& str);

    void error(const char* format, ...);
    void error(const std::string& str);

    void setInfoHandle(DumpHandler handle);
    void setWarningHandle(DumpHandler handle);
    void setErrorHandle(DumpHandler handle);

    void update();

    void enable(bool val) { _isEnable = val; }
    bool isEnable() const { return _isEnable; }

private:
    Dumper()
        : _isEnable(true)
        , _infoHandle(nullptr)
        , _warningHandle(nullptr)
        , _errorHandle(nullptr)
    {}

private:
    bool _isEnable;

    DumpHandler _infoHandle;
    DumpHandler _warningHandle;
    DumpHandler _errorHandle;

    SafeQueue<std::string> _infoQueue;
    SafeQueue<std::string> _warningQueue;
    SafeQueue<std::string> _errorQueue;
};