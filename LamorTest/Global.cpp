#include "Global.h"

#include <sstream>
#include <iomanip>
#include <chrono>

#include "Actions/ActionRunner.h"
#include "Actions/CommonAction.h"

std::string getDataTime()
{
    using namespace std::chrono;
    system_clock::time_point now = system_clock::now();
    milliseconds ms = duration_cast<milliseconds>(now.time_since_epoch()) % 1000;

    time_t tt = system_clock::to_time_t(now);
    tm* time_tm = localtime(&tt);

    std::ostringstream oss;
    oss << time_tm->tm_year + 1900 << "/" << std::setfill('0') << std::setw(2) << time_tm->tm_mon + 1 << "/" << std::setfill('0') << std::setw(2) << time_tm->tm_mday << " ";
    oss << std::setfill('0') << std::setw(2) << time_tm->tm_hour << ":" << std::setfill('0') << std::setw(2) << time_tm->tm_min << ":" << std::setfill('0') << std::setw(2) << time_tm->tm_sec << ".";
    oss << std::setfill('0') << std::setw(3) << ms.count();

    return oss.str();
}

std::string getDataTime2(int second)
{
    using namespace std::chrono;
    seconds tp_seconds(second);
    system_clock::time_point tp(tp_seconds);

    time_t tt = system_clock::to_time_t(tp);
    tm* time_tm = localtime(&tt);

    std::ostringstream oss;
    oss << time_tm->tm_year + 1900 << "/" << std::setfill('0') << std::setw(2) << time_tm->tm_mon + 1 << "/" << std::setfill('0') << std::setw(2) << time_tm->tm_mday << " ";
    oss << std::setfill('0') << std::setw(2) << time_tm->tm_hour << ":" << std::setfill('0') << std::setw(2) << time_tm->tm_min << ":" << std::setfill('0') << std::setw(2) << time_tm->tm_sec;

    return oss.str();
}

void logStart(const std::string& operation)
{
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("<b>" + operation + "</b>")));
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(getDataTime() + " Start")));
}