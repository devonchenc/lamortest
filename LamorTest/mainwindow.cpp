﻿#include <QApplication>
#include <QMessageBox>
#include <QKeyEvent>
#include <QDockWidget>
#include <QHBoxLayout>
#include <QToolBar>
#include <QFileDialog>

#include "mainwindow.h"
#include "Widget/InfoWidget.h"
#include "Widget/CommandWidget.h"
#include "Widget/SettingsDialog.h"

#include "Hardware/Board.h"
#include "Actions/ActionRunner.h"
#include "Actions/CommonAction.h"
#include "Actions/BoardAction.h"
#include "Dumper.h"

const int UPDATE_INTERVAL = 33;

MainWindow::MainWindow(QWidget* parent)
    : QMainWindow(parent)
{
    initUI();

    setWindowIcon(QIcon(":/images/logo.png"));

    connect(&_updateTimer, &QTimer::timeout, this, &MainWindow::onUpdateTimer);
    _updateTimer.start(UPDATE_INTERVAL);

    Dumper::instance().setInfoHandle(std::bind(&InfoWidget::onShowInfo, _infoWidget, std::placeholders::_1));
    Dumper::instance().setWarningHandle(std::bind(&InfoWidget::onShowWarning, _infoWidget, std::placeholders::_1));
    Dumper::instance().setErrorHandle(std::bind(&InfoWidget::onShowError, _infoWidget, std::placeholders::_1));

    ActionRunner::instance().setErrorHandler(std::bind(&InfoWidget::onShowErrorWithCode, _infoWidget, std::placeholders::_1, std::placeholders::_2));

    initBoard();
}

void MainWindow::initUI()
{
    createActions();

    createToolbar();

    _infoWidget = new InfoWidget;
    _commandWidget = new CommandWidget;
    connect(_commandWidget, &CommandWidget::saveXmlFile, _infoWidget, &InfoWidget::saveXmlFile);

    QHBoxLayout* mainLayout = new QHBoxLayout;
    mainLayout->addWidget(_infoWidget);
    mainLayout->addWidget(_commandWidget);
    mainLayout->setStretch(0, 1);
    mainLayout->setStretch(1, 0);

    QWidget* centerWidget = new QWidget;
    centerWidget->setLayout(mainLayout);
    setCentralWidget(centerWidget);
}

void MainWindow::createActions()
{
    _openAction = new QAction(tr("Open"), this);
    _openAction->setIcon(QIcon(":/images/open.png"));
    _saveAction = new QAction(tr("Save"), this);
    _saveAction->setIcon(QIcon(":/images/save.png"));
    _testAction = new QAction(tr("Test"), this);
    _testAction->setIcon(QIcon(":/images/test.png"));
    _settingsAction = new QAction(tr("Settings"), this);
    _settingsAction->setIcon(QIcon(":/images/settings.png"));
    _exitAction = new QAction(tr("Exit"), this);
    _exitAction->setIcon(QIcon(":/images/exit.png"));

    connect(_openAction, &QAction::triggered, this, &MainWindow::open);
    connect(_saveAction, &QAction::triggered, this, &MainWindow::save);
    connect(_testAction, &QAction::triggered, this, &MainWindow::test);
    connect(_settingsAction, &QAction::triggered, this, &MainWindow::setting);
    connect(_exitAction, &QAction::triggered, this, &MainWindow::quit);
}

void MainWindow::createToolbar()
{
    setIconSize(QSize(40, 40));

    _toolBar = new QToolBar(tr("Toolbar"), this);
    _toolBar->setToolButtonStyle(Qt::ToolButtonTextBesideIcon);
    addToolBar(_toolBar);

    _toolBar->addAction(_openAction);
    _toolBar->addAction(_saveAction);
    _toolBar->addSeparator();
    _toolBar->addAction(_testAction);
    _toolBar->addSeparator();
    _toolBar->addAction(_settingsAction);
    _toolBar->addSeparator();
    _toolBar->addAction(_exitAction);
}

void MainWindow::open()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open Instruction File"), "/", tr("Xml file (*.xml)"));
    if (!fileName.isEmpty())
    {
        _infoWidget->openFile(fileName);
    }
}

void MainWindow::save()
{
    QString fileName = QFileDialog::getSaveFileName(this, tr("Save Instruction File"), "/", tr("Xml file (*.xml)"));
    if (!fileName.isEmpty())
    {
        _infoWidget->saveFile(fileName);
    }
}

void MainWindow::test()
{
    // 通讯接口通讯质量测试
    ActionRunner::instance().runAction(ActionPtr(new ComTestAction));

    // 信息读取测试
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::DeviceType)));
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::HardwareVersion)));
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::SoftwareVersion)));
    ActionRunner::instance().runAction(ActionPtr(new ReadDeviceInfoAction(InfoType::RAMVersion)));
    ActionRunner::instance().runAction(ActionPtr(new ReadAuxInfoAction));

    // 读写时间测试
    ActionRunner::instance().runAction(ActionPtr(new SyncTimeAction));
    ActionRunner::instance().runAction(ActionPtr(new ReadTimeAction(true)));

    // RAM测试
    int ramAddress = 0;
    int dataLen = 512;
    std::vector<int> data;
    ActionRunner::instance().runAction(ActionPtr(new WriteRamDataAction(ramAddress, data)));
    ActionRunner::instance().runAction(ActionPtr(new ReadRamDataAction(ramAddress, dataLen)));

    // DVP模式测试
    ActionRunner::instance().runAction(ActionPtr(new EnterDVPModeAction));
    ActionRunner::instance().runAction(ActionPtr(new ExitDVPModeAction));

    // 写FIR参数、写CIC参数测试
    std::vector<int> value
    {
        2,2,3,4,6,8,10,12, 
        15,19,23,27,32,37,42,48,
        54,61,67,74,81,87,93,99,
        105,110,115,119,122,124,126,127,
        127,126,124,122,119,115,110,105,
        99,93,87,81,74,67,61,54,
        48,42,37,32,27,23,19,15,
        12,10,8,6,4,3,2,2
    };
    ActionRunner::instance().runAction(ActionPtr(new WriteFIRParameterAction(value)));
    ActionRunner::instance().runAction(ActionPtr(new WriteCICParameterAction(1)));

    // 自发自收测试
    std::vector<std::map<std::string, std::string>> timecore;
    {
        // TXS指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "TXS"));
        parameters.insert(std::pair<std::string, std::string>("dacen", "1"));
        parameters.insert(std::pair<std::string, std::string>("tx_phase", "0"));
        parameters.insert(std::pair<std::string, std::string>("frequency", "10"));
        parameters.insert(std::pair<std::string, std::string>("dac_ratio", "1"));
        parameters.insert(std::pair<std::string, std::string>("load", "1"));
        timecore.push_back(parameters);
    }
    {
        // WAIT指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "WAIT"));
        parameters.insert(std::pair<std::string, std::string>("dataT", "1"));
        timecore.push_back(parameters);
    }
    {
        // RXLP指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "RXLP"));
        parameters.insert(std::pair<std::string, std::string>("load", "1"));
        parameters.insert(std::pair<std::string, std::string>("frequency", "0"));
        parameters.insert(std::pair<std::string, std::string>("rx_phase", "0"));
        parameters.insert(std::pair<std::string, std::string>("acq_number", "1000"));
        timecore.push_back(parameters);
    }
    {
        // RXE指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "RXE"));
        parameters.insert(std::pair<std::string, std::string>("rxen0", "1"));
        parameters.insert(std::pair<std::string, std::string>("rxen1", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen2", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen3", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen4", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen5", "0"));
        timecore.push_back(parameters);
    }
    {
        // WAIT指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "WAIT"));
        parameters.insert(std::pair<std::string, std::string>("dataT", "16"));
        timecore.push_back(parameters);
    }
    {
        // RXE指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "RXE"));
        parameters.insert(std::pair<std::string, std::string>("rxen0", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen1", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen2", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen3", "0"));
        parameters.insert(std::pair<std::string, std::string>("rxen4", "1"));
        parameters.insert(std::pair<std::string, std::string>("rxen5", "0"));
        timecore.push_back(parameters);
    }
    {
        // TXS指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "TXS"));
        parameters.insert(std::pair<std::string, std::string>("dacen", "0"));
        parameters.insert(std::pair<std::string, std::string>("tx_phase", "0"));
        parameters.insert(std::pair<std::string, std::string>("frequency", "10"));
        parameters.insert(std::pair<std::string, std::string>("dac_ratio", "1"));
        parameters.insert(std::pair<std::string, std::string>("load", "0"));
        timecore.push_back(parameters);
    }
    {
        // END指令
        std::map<std::string, std::string> parameters;
        parameters.insert(std::pair<std::string, std::string>("TimeCoreType", "END"));
        timecore.push_back(parameters);
    }
    int address = 0;
    ActionRunner::instance().runAction(ActionPtr(new WriteTimeCoreParameterAction(address, timecore)));

    // 通过Start Acquire指令启动时间核代码
    ActionRunner::instance().runAction(ActionPtr(new StartAcquireAction));

    // 通过Read Status Word指令查询设备状态，当设备处于idle/none状态时，可以读出RAM数据
    ActionRunner::instance().runAction(ActionPtr(new ReadStatusAction));

    // 分析数据频率和幅值是否正确
}

void MainWindow::setting()
{
    SettingsDialog dlg;
    if (dlg.exec() == QDialog::Accepted)
    {
        ActionRunner::instance().runAction(ActionPtr(new CloseBoardAction));

        logStart("[Connect]");
        ActionRunner::instance().runAction(ActionPtr(new OpenBoardAction));
    }
}

void MainWindow::quit()
{
    close();
}

void MainWindow::initBoard()
{
    logStart("[Initiate]");
    logStart("[Connect]");
    ActionRunner::instance().runAction(ActionPtr(new OpenBoardAction));
}

void MainWindow::onUpdateTimer()
{
    BoardStation::instance().update(UPDATE_INTERVAL);
    ActionRunner::instance().update(UPDATE_INTERVAL);

    Dumper::instance().update();
}

void MainWindow::closeEvent(QCloseEvent* event)
{
    QMessageBox::StandardButton button;
#ifdef _DEBUG
    button = QMessageBox::Yes;
#else
    button = QMessageBox::question(this, "LamorTest", tr("Are you sure you want to exit the software?"));
#endif

    if (button == QMessageBox::Yes)
    {
        _updateTimer.stop();

        ActionRunner::instance().cancel();

        BoardStation::instance().close();

        event->accept();
    }
    else
    {
        event->ignore();
    }
}