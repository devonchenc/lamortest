#include "Dumper.h"

#include <stdarg.h>

const int BUFFER_LEN = 256;

Dumper& Dumper::instance()
{
    static Dumper instance;
    return instance;
}

void Dumper::info(const char* format, ...)
{
	if (_isEnable)
    {
        char buffer[BUFFER_LEN];
        va_list args;
        va_start(args, format);
        _vsnprintf_s(buffer, BUFFER_LEN, format, args);
        va_end(args);

        _infoQueue.push(buffer);
	}
}

void Dumper::info(const std::string& str)
{
    _infoQueue.push(str);
}

void Dumper::warning(const char* format, ...)
{
    char buffer[BUFFER_LEN];
    va_list args;
    va_start(args, format);
    _vsnprintf_s(buffer, BUFFER_LEN, format, args);
    va_end(args);

    _warningQueue.push(buffer);
}

void Dumper::warning(const std::string& str)
{
    _warningQueue.push(str);
}

void Dumper::error(const char* format, ...)
{
    char buffer[BUFFER_LEN];
    va_list args;
    va_start(args, format);
    _vsnprintf_s(buffer, BUFFER_LEN, format, args);
    va_end(args);

    _errorQueue.push(buffer);
}

void Dumper::error(const std::string& str)
{
    _errorQueue.push(str);
}

void Dumper::setInfoHandle(DumpHandler handle)
{
    _infoHandle = handle;
}

void Dumper::setWarningHandle(DumpHandler handle)
{
    _warningHandle = handle;
}

void Dumper::setErrorHandle(DumpHandler handle)
{
    _errorHandle = handle;
}

void Dumper::update()
{
    while (_isEnable && !_infoQueue.empty())
    {
        std::string info = _infoQueue.pop();
        if (_infoHandle)
        {
            _infoHandle(info);
        }
    }

    while (!_warningQueue.empty())
    {
        std::string warning = _warningQueue.pop();
        if (_warningHandle)
        {
            _warningHandle(warning);
        }
    }

    while (!_errorQueue.empty())
    {
        std::string err = _errorQueue.pop();
        if (_errorHandle)
        {
            _errorHandle(err);
        }
    }
}