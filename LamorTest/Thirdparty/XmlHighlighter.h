#pragma once

#include <QSyntaxHighlighter>
#include <QObject>

class XmlHighlighter : public QSyntaxHighlighter
{
    Q_OBJECT

public:
    explicit XmlHighlighter(QTextDocument* parent = nullptr);

protected:
    void highlightBlock(const QString &text);

private:
    struct HighlightingRule
    {
        QRegExp pattern;
        QTextCharFormat format;
    };
    QVector<HighlightingRule> highlightingRules;

    QRegExp commentStartExpression;
    QRegExp commentEndExpression;
    
    QTextCharFormat multiLineCommentFormat;
    QTextCharFormat quotationFormat;
    QTextCharFormat elementNameFormat;
    QTextCharFormat propertyFormat;
};
