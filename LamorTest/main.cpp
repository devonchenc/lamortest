﻿#include <iostream>

#include "Thirdparty/qtsingleapplication.h"
#include "mainwindow.h"

int main(int argc, char* argv[])
{
    QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);
    QCoreApplication::setAttribute(Qt::AA_UseHighDpiPixmaps);

    QtSingleApplication app(argc, argv);
    if (app.isRunning())
    {
        app.sendMessage("wakeup");
        return EXIT_SUCCESS;
    }

    MainWindow mainWin;
    app.setActivationWindow(&mainWin);
    QObject::connect(&app, &QtSingleApplication::messageReceived, &app, &QtSingleApplication::activateWindow);
    mainWin.resize(QSize(1200, 660));
    mainWin.show();

    return app.exec();
}
