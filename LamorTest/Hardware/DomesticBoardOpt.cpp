#include "DomesticBoardOpt.h"

#include <iostream>
#include <sstream>
#include <random>
#include <ctime>
#include <chrono>
#include <limits.h>
#include <assert.h>

#include "../Actions/ActionRunner.h"
#include "../Actions/CommonAction.h"

#define M_PI       3.14159265358979323846   // pi

ReadDeviceInfoOpt::ReadDeviceInfoOpt(SerialPortImpl* comHandle, InfoType infoType, CallbackStringVector onReply)
    : SerialPortOpt(INSB_READ_DEVICE_INFO, 1, comHandle)
    , _infoType(infoType)
    , _replyEvent(onReply)
{

}

void ReadDeviceInfoOpt::onExecute()
{
    logStart("[Read Device Info]");

    switch (_infoType)
    {
    case DeviceType:
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Mode = Instrument model")));
    }
    break;
    case HardwareVersion:
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Mode = Control circuit hardware version number")));
    }
    break;
    case SoftwareVersion:
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Mode = Control circuit software version number")));
    }
    break;
    case RAMVersion:
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Mode = RAM version number")));
    }
    break;
    }

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = _infoType;

    sendAndReceive();
}

void ReadDeviceInfoOpt::onReply()
{
    if (_errorCode == 0)
    {
        if (_replyEvent)
        {
            _replyEvent(_vector);
        }
    }
    else
    {
        if (_errorCode == 0x8000)
        {
            // Info Type不存在
        }
    }
}

bool ReadDeviceInfoOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read Device Info = Fail, Error Code = " + oss.str())));
        return false;
    }

    _vector.clear();

    unsigned short* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;

    switch (_infoType)
    {
    case DeviceType:
    {
        unsigned short deviceTypeLength = *(buffer++);
        std::string info = extractString(buffer, deviceTypeLength);
        _vector.push_back(info);

        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("DeviceTpye = " + info)));
    }
    break;
    case HardwareVersion:
    {
        unsigned short comBoardVersionLength = *(buffer++);
        std::string comBoardVersion = extractString(buffer, comBoardVersionLength);
        _vector.push_back(comBoardVersion);

        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("ComBoardVersion = " + comBoardVersion)));

        buffer += comBoardVersionLength;
        unsigned short motherBoardVersionLength = *(buffer++);
        std::string motherBoardVersion = extractString(buffer, motherBoardVersionLength);
        _vector.push_back(motherBoardVersion);

        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("MotherBoardVersion = " + motherBoardVersion)));

        buffer += motherBoardVersionLength;
        unsigned short coreBoardVersionLength = *(buffer++);
        std::string coreBoardVersion = extractString(buffer, coreBoardVersionLength);
        _vector.push_back(coreBoardVersion);

        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("CoreBoardVersion = " + coreBoardVersion)));
    }
    break;
    case SoftwareVersion:
    {
        unsigned short comBoardVersionLength = *(buffer++);
        std::string comBoardVersion = extractString(buffer, comBoardVersionLength);
        _vector.push_back(comBoardVersion);

        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("ComBoardVersion = " + comBoardVersion)));

        buffer += comBoardVersionLength;
        unsigned short coreBoardVersionLength = *(buffer++);
        std::string coreBoardVersion = extractString(buffer, coreBoardVersionLength);
        _vector.push_back(coreBoardVersion);

        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("CoreBoardVersion = " + coreBoardVersion)));
    }
    break;
    case RAMVersion:
    {
        unsigned short ramVersionLength = *(buffer++);
        unsigned short ramType = *(buffer++);
        unsigned short storageDepth1 = *(buffer++);
        unsigned short storageDepth2 = *(buffer++);
        unsigned short bitWdith = *(buffer++);
        std::string ramID = extractString(buffer, ramVersionLength - 4);
        _vector.push_back(ramID);

        std::ostringstream oss;
        oss << "RAM Type = ";
        switch (ramType)
        {
        case 1:
            oss << "DDR";
            break;
        case 2:
            oss << "DDR II";
            break;
        case 3:
            oss << "DDR III";
            break;
        case 4:
            oss << "DDR IV";
            break;
        case 5:
            oss << "DDR V";
            break;
        case 6:
            oss << "SRAM";
            break;
        case 7:
            oss << "DSRAM";
            break;
        default:
            oss << "UNKNOWN";
            break;
        }
        oss << ", StorageDepth = " << storageDepth1 << storageDepth2 << ", BitWidth = " << bitWdith;
        oss << ", RAM ID = " << ramID;
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));
    }
    break;
    }

    return true;
}

//////////////////////////////////////////////////////////////////////////

ChangeBaudrateOpt::ChangeBaudrateOpt(SerialPortImpl* comHandle, int baudrate, CallbackVoid onReply)
    : SerialPortOpt(INSB_CHANGE_BAUDRATE, 2, comHandle)
    , _baudrate(baudrate)
    , _replyEvent(onReply)
{

}

void ChangeBaudrateOpt::onExecute()
{
    logStart("[Change Baudrate]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0;
    _instructionBuffer[3] = _baudrate / 100;

    sendAndReceive();
}

void ChangeBaudrateOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ChangeBaudrateOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Change Baudrate = Fail, Error Code = " + oss.str())));
        return false;
    }

    unsigned short* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    unsigned short changeFlag = *(buffer++);        // 1.设备接收并改变波特率, 2.设备不改变波特率
    unsigned short receivedBaudrateHigh = *(buffer++);  // 接收到的波特率
    unsigned short receivedBaudrateLow = *(buffer++);
    unsigned short newBaudrateHigh = *(buffer++);      // 设备新的波特率
    unsigned short newBaudrateLow = *(buffer++);
    int receivedBaudrate = int(receivedBaudrateHigh << 16) | int(receivedBaudrateLow);
    int newBaudrate = int(newBaudrateHigh << 16) | int(newBaudrateLow);

    std::ostringstream oss;
    oss << "Change Flag = ";
    if (changeFlag == 1)
    {
        oss << "true";
    }
    else if (changeFlag == 2)
    {
        oss << "false";
    }
    else
    {
        oss << "unknown";
    }
    oss << ", ReceivedBaudrate = " << receivedBaudrate * 100;
    oss << ", NewBaudrate = " << newBaudrate * 100;

    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    return true;
}

//////////////////////////////////////////////////////////////////////////

ComTestOpt::ComTestOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_COM_TEST, 0, comHandle)
    , _replyEvent(onReply)
{

}

void ComTestOpt::onExecute()
{
    logStart("[COM Test]");

    // 产生随机数作为数据长度(50-400)
    std::default_random_engine e(static_cast<unsigned int>(time(nullptr)));
    std::uniform_int_distribution<int> distribInt(50, 400);
    _sendDataLen = distribInt(e);

    // 重新分配内存
    allocateBuffer();

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    // 随机填充
    std::uniform_int_distribution<unsigned short> distribShortInt(0, USHRT_MAX);
    for (int i = 0; i < _sendDataLen; i++)
    {
        _instructionBuffer[i + INSTRUCTION_DATA_OFFSET] = distribShortInt(e);
    }

    sendAndReceive();
}

void ComTestOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ComTestOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("COM Test = Fail, Error Code = " + oss.str())));
        return false;
    }

    unsigned short* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    // Compare received random data
    for (int i = 0; i < _responseDataLen - RESPONSE_DATA_OFFSET - 1; i++)
    {
        if (buffer[i] != _instructionBuffer[i + INSTRUCTION_DATA_OFFSET])
        {
            ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("COM Test = Fail, the received data doesn't match the sent data")));
            return false;
        }
    }

    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("COM Test Success")));

    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadStatusOpt::ReadStatusOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_READ_STATUS, 0, comHandle)
    , _statusWordHigh(0)
    , _statusWordLow(0)
    , _replyEvent(onReply)
{

}

void ReadStatusOpt::onExecute()
{
    logStart("[Read Status Word]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

void ReadStatusOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadStatusOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read Status = Fail, Error Code = " + oss.str())));
        return false;
    }

    _statusWordHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    _statusWordLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];

    int topLoopNum = _statusWordHigh;
    unsigned short FPGAState = _statusWordLow & 0x000F;
    unsigned short FPGACfg = (_statusWordLow & 0x0010) >> 4;
    unsigned short FPGAPLL = (_statusWordLow & 0x0020) >> 5;

    std::ostringstream oss;
    oss << "TopLoopNum = " << topLoopNum << ", FPGA PLL = ";
    if (FPGAPLL == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    oss << ", FPGA Cfg = ";
    if (FPGACfg == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    oss << ", FPGAState = ";
    switch (FPGAState)
    {
    case 0:
        oss << "idle/none";
        break;
    case 1:
        oss << "transmit";
        break;
    case 2:
        oss << "receive";
        break;
    case 3:
        oss << "transcive";
        break;
    case 4:
        oss << "wait";
        break;
    case 5:
        oss << "rdwrram";
        break;
    case 6:
        oss << "program";
        break;
    default:
        oss << "invalid";
        break;
    }
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    return true;
}

//////////////////////////////////////////////////////////////////////////

ClearAllErrorOpt::ClearAllErrorOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_CLEAR_ALL_ERROR, 1, comHandle)
    , _replyEvent(onReply)
{

}

void ClearAllErrorOpt::onExecute()
{
    logStart("[Clear All Error]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0505;

    sendAndReceive();
}

void ClearAllErrorOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

//////////////////////////////////////////////////////////////////////////

SyncTimeOpt::SyncTimeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_SYNC_TIME, 2, comHandle)
    , _replyEvent(onReply)
{

}

void SyncTimeOpt::onExecute()
{
    logStart("[Sync Time]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    using namespace std::chrono;
    system_clock::time_point now = system_clock::now();
    // 获取1970.1.1以来的秒数
    seconds timeSinceEpoch = duration_cast<seconds>(now.time_since_epoch());
    int second = timeSinceEpoch.count();
    _instructionBuffer[2] = second >> 16;           // Timestamp high word
    _instructionBuffer[3] = second & 0x0000FFFF;    // Timestamp low word

    sendAndReceive();
}

void SyncTimeOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool SyncTimeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Sync Time = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Sync Time = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadTimeOpt::ReadTimeOpt(SerialPortImpl* comHandle, CallbackInt onReply)
    : SerialPortOpt(INSB_READ_TIME, 0, comHandle)
    , _readTime(0)
    , _replyEvent(onReply)
{

}

void ReadTimeOpt::onExecute()
{
    logStart("[Read Time]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

void ReadTimeOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent(_readTime);
    }
}

bool ReadTimeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read Time = Fail, Error Code = " + oss.str())));
        return false;
    }

    unsigned short timeStampHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    unsigned short timeStampLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];
    _readTime = int(timeStampHigh << 16) | int(timeStampLow);

    std::ostringstream oss;
    oss << "TimeValue = " << time;
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Time = " + getDataTime2(_readTime))));

    return true;
}

//////////////////////////////////////////////////////////////////////////

WriteFIRParameterOpt::WriteFIRParameterOpt(SerialPortImpl* comHandle, const std::vector<int>& value, CallbackVoid onReply)
    : SerialPortOpt(INSB_WRITE_PARA, 65, comHandle)
    , _value(value)
    , _replyEvent(onReply)
{
    // FIR滤波器参数最多64个
    assert(_value.size() <= 64);
}

void WriteFIRParameterOpt::onExecute()
{
    logStart("[Write FIR Parameter]");

    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("ParaType = FIR")));
    std::ostringstream oss;
    oss << "Parameters = ";
    for (int i = 0; i < _value.size(); i++)
    {
        oss << _value[i];
        if (i < _value.size() - 1)
        {
            oss << ", ";
        }
    }
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = ParaType::FIR;

    // 填充FIR滤波器参数
    for (int i = 0; i < _value.size(); i++)
    {
        _instructionBuffer[3 + i] = _value[i];
    }

    sendAndReceive();
}

void WriteFIRParameterOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool WriteFIRParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Write FIR Parameter = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Write FIR Parameter = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

WriteCICParameterOpt::WriteCICParameterOpt(SerialPortImpl* comHandle, unsigned short value, CallbackVoid onReply)
    : SerialPortOpt(INSB_WRITE_PARA, 2, comHandle)
    , _value(value)
    , _replyEvent(onReply)
{

}

void WriteCICParameterOpt::onExecute()
{
    logStart("[Write CIC Parameter]");

    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("ParaType = CIC")));
    std::ostringstream oss;
    oss << "Parameter = " << _value;
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = ParaType::CIC;
    _instructionBuffer[3] = _value;

    sendAndReceive();
}

void WriteCICParameterOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool WriteCICParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Write CIC Parameter = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Write CIC Parameter = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

WriteTimeCoreParameterOpt::WriteTimeCoreParameterOpt(SerialPortImpl* comHandle, int address, const std::vector<TimeCore>& parameters, CallbackVoid onReply)
    : SerialPortOpt(INSB_WRITE_PARA, 3 + parameters.size() * 4, comHandle)
    , _address(address)
    , _parameters(parameters)
    , _replyEvent(onReply)
{

}

void WriteTimeCoreParameterOpt::onExecute()
{
    logStart("[Write TimeCore Parameter]");

    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("ParaType = TimeCore")));
    for (int i = 0; i < _parameters.size(); i++)
    {
        std::map<std::string, std::string>::iterator iter = _parameters[i].find("TimeCoreType");
        if (iter == _parameters[i].end())
            continue;

        std::ostringstream oss;
        oss << iter->first << " = " << iter->second;

        for (iter = _parameters[i].begin(); iter != _parameters[i].end(); iter++)
        {
            if (iter->first != "TimeCoreType")
            {
                oss << ", " << iter->first << " = " << iter->second;
            }
        }
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));
    }

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = ParaType::TIMECORE;
    _instructionBuffer[3] = _address & 0x0000FFFF;   // TimeCore写入地址
    _instructionBuffer[4] = _address << 16;
    // 写入TimeCore
    int bufferOffset = 5;
    for (int i = 0; i < _parameters.size(); i++)
    {
        if (translatedintoCode(_parameters[i], _instructionBuffer + bufferOffset))
        {
            bufferOffset += 4;
        }
    }

    sendAndReceive();
}

bool WriteTimeCoreParameterOpt::translatedintoCode(const TimeCore& timeCore, unsigned short* buffer)
{
    std::map<std::string, std::string>::const_iterator iter = timeCore.find("TimeCoreType");
    if (iter == timeCore.end())
        return false;

    // 时间核指令
    std::string instruction = iter->second;
    if (instruction == "TXS")
    {
        buffer[0] = TimeCoreType::TXS;
        int dacen = 0;
        float tx_phase = 0;
        float frequency = 0;
        float dac_ratio = 0;
        int load = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "dacen")
            {
                ss >> dacen;
            }
            else if (iter->first == "tx_phase")
            {
                ss >> tx_phase;
            }
            else if (iter->first == "frequency")
            {
                ss >> frequency;
            }
            else if (iter->first == "dac_ratio")
            {
                ss >> dac_ratio;
            }
            else if (iter->first == "load")
            {
                ss >> load;
            }
        }

        // 计算freq_bits
        int freq_bits = (frequency / 75.0f) * (1 << 24);
        int tx_phase_bits = tx_phase * (1 << 12) / (2 * M_PI);
        int dac_ration_bits = dac_ratio * ((1 << 7) + 1);

        buffer[1] |= dacen & 0b1;                   // dacen
        buffer[1] |= (load & 0b1) << 1;             // load
        buffer[1] |= (tx_phase_bits & 0xFFF) << 4;  // tx_phase[11:0]
        buffer[2] |= dac_ration_bits & 0xFF;        // dac_ration[7:0]
        buffer[2] |= (freq_bits & 0xFF) << 8;       // freq_bits[7:0]
        buffer[3] |= (freq_bits & 0xFFFF00) >> 8;   // freq_bits[23:8]
    }
    else if (instruction == "WAIT")
    {
        buffer[0] = TimeCoreType::WAIT;
        int delayTime = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "dataT")
            {
                ss >> delayTime;
            }
        }

        // 将时间us转换为周期数
        int dataT = delayTime * 75;

        buffer[0] |= (dataT & 0x000F) << 12;        // dataT[3:0]
        buffer[1] |= (dataT & 0xFFFF0) >> 4;        // dataT[19:4]
    }
    else if (instruction == "LOOPS")
    {
        buffer[0] = TimeCoreType::LOOPS;
        int rloopsel = 0;
        int dataR = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "Rloopsel")
            {
                ss >> rloopsel;
            }
            else if (iter->first == "dataR")
            {
                ss >> dataR;
            }
        }

        buffer[0] |= (rloopsel & 0x000F) << 4;      // Rloopsel[3:0]
        buffer[0] |= (dataR & 0x000F) << 12;        // dataR[3:0]
        buffer[1] |= (dataR & 0xFFFF0) >> 4;        // dataR[19:4]
    }
    else if (instruction == "LOOPE")
    {
        buffer[0] = TimeCoreType::LOOPE;
        int rloopsel = 0;
        int addra = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "Rloopsel")
            {
                ss >> rloopsel;
            }
            else if (iter->first == "addra")
            {
                ss >> addra;
            }
        }

        buffer[0] |= (rloopsel & 0x000F) << 4;      // Rloopsel[3:0]
        buffer[1] |= addra & 0xFFFF;                // addra[15:0]
    }
    else if (instruction == "RXE")
    {
        buffer[0] = TimeCoreType::RXE;
        int rxen0 = 0;
        int rxen1 = 0;
        int rxen2 = 0;
        int rxen3 = 0;
        int rxen4 = 0;
        int rxen5 = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "rxen0")
            {
                ss >> rxen0;
            }
            else if (iter->first == "rxen1")
            {
                ss >> rxen1;
            }
            else if (iter->first == "rxen2")
            {
                ss >> rxen2;
            }
            else if (iter->first == "rxen3")
            {
                ss >> rxen3;
            }
            else if (iter->first == "rxen4")
            {
                ss >> rxen4;
            }
            else if (iter->first == "rxen5")
            {
                ss >> rxen5;
            }
        }

        buffer[2] |= rxen0 & 0b1;                   // rexn[0]
        buffer[2] |= (rxen1 & 0b1) << 1;            // rexn[1]
        buffer[2] |= (rxen2 & 0b1) << 2;            // rexn[2]
        buffer[2] |= (rxen3 & 0b1) << 3;            // rexn[3]
        buffer[2] |= (rxen4 & 0b1) << 4;            // rexn[4]
        buffer[2] |= (rxen5 & 0b1) << 5;            // rexn[5]
    }
    else if (instruction == "OTL")
    {
        buffer[0] = TimeCoreType::OTL;
        int pamp_ctr = 0;
        int outer_ctrl = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "pamp_ctr")
            {
                ss >> pamp_ctr;
            }
            else if (iter->first == "outer_ctrl")
            {
                ss >> outer_ctrl;
            }
        }

        buffer[2] |= pamp_ctr & 0b1;                // pamp_ctr
        buffer[2] |= (outer_ctrl & 0xF) << 1;       // rexn[1]
    }
    else if (instruction == "RXLP")
    {
        buffer[0] = TimeCoreType::RXLP;
        int load = 0;
        float frequency = 0;
        float rx_phase = 0;
        int acq_number = 0;
        for (iter = timeCore.begin(); iter != timeCore.end(); iter++)
        {
            std::stringstream ss(iter->second);
            if (iter->first == "load")
            {
                ss >> load;
            }
            else if (iter->first == "frequency")
            {
                ss >> frequency;
            }
            else if (iter->first == "rx_phase")
            {
                ss >> rx_phase;
            }
            else if (iter->first == "acq_number")
            {
                ss >> acq_number;
            }
        }

        // 计算freq_bits
        int freq_bits = (frequency / 75.0f) * (1 << 24);
        int rx_phase_bits = rx_phase * (1 << 12) / (2 * M_PI);

        buffer[0] |= (load & 0b1) << 4;             // load
        buffer[0] |= (acq_number & 0xFF) << 8;      // acq_number[7:0]
        buffer[1] |= (acq_number & 0xFFF00) >> 8;   // acq_number[19:8]
        buffer[1] |= (rx_phase_bits & 0xF) << 12;   // rx_phase_bits[3:0]
        buffer[2] |= (rx_phase_bits & 0xFF0) >> 4;  // rx_phase_bits[11:4]
        buffer[2] |= (freq_bits & 0xFF) << 8;       // freq_bits[7:0]
        buffer[3] |= (freq_bits & 0xFFFF00) >> 8;   // freq_bits[23:8]
    }
    else if (instruction == "END")
    {
        buffer[0] = TimeCoreType::END;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Wrong TimeCore Instruction")));
    }

    return true;
}

void WriteTimeCoreParameterOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool WriteTimeCoreParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Write TimeCore Parameter = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Write TimeCore Parameter = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadParameterOpt::ReadParameterOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_READ_PARA, 0, comHandle)
    , _replyEvent(onReply)
{

}

void ReadParameterOpt::onExecute()
{
    logStart("[Read Parameter]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

void ReadParameterOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadParameterOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read Parameter = Fail, Error Code = " + oss.str())));
        return false;
    }

    // 解析读取到的参数

    return true;
}

//////////////////////////////////////////////////////////////////////////

StartAcquireOpt::StartAcquireOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_START_ACQUIRE, 1, comHandle)
    , _replyEvent(onReply)
{

}

void StartAcquireOpt::onExecute()
{
    logStart("[Start Acquire]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0A0A;

    sendAndReceive();
}

void StartAcquireOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool StartAcquireOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        if (_errorCode == 0x0200)
        {
            // 指令执行失败
        }
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Start Acquire = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Start Acquire = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadAcquiredDataOpt::ReadAcquiredDataOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_READ_ACQUIRED_DATA, 0, comHandle)
    , _replyEvent(onReply)
{

}

void ReadAcquiredDataOpt::onExecute()
{
    logStart("[Read Acquired Data]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;

    sendAndReceive();
}

void ReadAcquiredDataOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadAcquiredDataOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read Acquired Data = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Read Acquire = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

WriteRamDataOpt::WriteRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, const std::vector<int>& data, CallbackVoid onReply)
    : SerialPortOpt(INSB_WRITE_RAW_DATA, 4 + data.size() * 2, comHandle)
    , _ramAddress(ramAddress)
    , _data(data)
    , _replyEvent(onReply)
{

}

void WriteRamDataOpt::onExecute()
{
    logStart("[Write RAM Data]");

    std::ostringstream oss;
    oss << "RAM address = " << _ramAddress;
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));
    oss.str("");        // Clear oss
    oss << "RAM Data = [";
    for (int i = 0; i < _data.size(); i++)
    {
        oss << _data[i];
        if (i < _data.size() - 1)
        {
            oss << ", ";
        }
    }
    oss << "]";
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = (_ramAddress >> 48) & 0x0000FFFF;
    _instructionBuffer[3] = (_ramAddress >> 32) & 0x0000FFFF;
    _instructionBuffer[4] = (_ramAddress >> 16) & 0x0000FFFF;
    _instructionBuffer[5] = _ramAddress & 0x0000FFFF;
    for (int i = 0; i < _data.size(); i++)
    {
        _instructionBuffer[6 + i * 2] = _data[i] & 0x0000FFFF;  // Low word
        _instructionBuffer[6 + i * 2 + 1] = _data[i] >> 16;     // High word
    }

    sendAndReceive();
}

void WriteRamDataOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool WriteRamDataOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Write RAM Data = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Write RAM Data = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ReadRamDataOpt::ReadRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, int readLen, CallbackVoid onReply)
    : SerialPortOpt(INSB_READ_RAW_DATA, 5, comHandle)
    , _ramAddress(ramAddress)
    , _readLen(readLen)
    , _replyEvent(onReply)
{

}

void ReadRamDataOpt::onExecute()
{
    logStart("[Read RAM Data]");

    std::ostringstream oss;
    oss << "RAM address = " << _ramAddress;
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));
    oss.str("");
    oss << "ReadDataLength = " << _readLen;
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = (_ramAddress >> 48) & 0x0000FFFF;
    _instructionBuffer[3] = (_ramAddress >> 32) & 0x0000FFFF;
    _instructionBuffer[4] = (_ramAddress >> 16) & 0x0000FFFF;
    _instructionBuffer[5] = _ramAddress & 0x0000FFFF;
    _instructionBuffer[6] = _readLen;

    sendAndReceive();
}

void ReadRamDataOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadRamDataOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read RAM Data = Fail, Error Code = " + oss.str())));
        return false;
    }

    unsigned short dataLength = *_responseBuffer;
    unsigned short* buffer = _responseBuffer + RESPONSE_DATA_OFFSET;
    std::ostringstream oss;
    for (int i = 0; i < dataLength - 2; i+=2)
    {
        int value = int(buffer[i +1] << 16) | int(buffer[i]);
        oss << value;
        if (i < dataLength - 4)
        {
            oss << ", ";
        }
    }
    oss << "]";
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("RAM Data = [" + oss.str())));

    return true;
}

//////////////////////////////////////////////////////////////////////////

EnterDVPModeOpt::EnterDVPModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_ENTER_DVP_MODE, 1, comHandle)
    , _replyEvent(onReply)
{

}

void EnterDVPModeOpt::onExecute()
{
    logStart("[Enter DVP Mode]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0E0E;

    sendAndReceive();
}

void EnterDVPModeOpt::onReply()
{
    if (_errorCode == 0x8000)
    {
        // 收到的data1数据错误
    }
    else if (_errorCode == 0x0100)
    {
        // 进入开发模式失败
    }

    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool EnterDVPModeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Enter DVP Mode = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Enter DVP Mode = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

ExitDVPModeOpt::ExitDVPModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_EXIT_DVP_MODE, 1, comHandle)
    , _replyEvent(onReply)
{

}

void ExitDVPModeOpt::onExecute()
{
    logStart("[Exit DVP Mode]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0F0F;

    sendAndReceive();
}

void ExitDVPModeOpt::onReply()
{
    if (_errorCode == 0x8000)
    {
        // 退出开发模式失败
    }

    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ExitDVPModeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Exit DVP Mode = Fail, Error Code = " + oss.str())));
        return true;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Exit DVP Mode = Success")));
        return true;
    }
}

//////////////////////////////////////////////////////////////////////////

TerminateExeOpt::TerminateExeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_TEMINATE_EXE, 1, comHandle)
    , _replyEvent(onReply)
{

}

void TerminateExeOpt::onExecute()
{
    logStart("[Terminate Exe]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x0101;

    sendAndReceive();
}

void TerminateExeOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool TerminateExeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Terminate Exe = Fail, Error Code = " + oss.str())));
        return false;
    }

    unsigned short statusWordHigh = _responseBuffer[RESPONSE_DATA_OFFSET];
    unsigned short statusWordLow = _responseBuffer[RESPONSE_DATA_OFFSET + 1];

    int topLoopNum = statusWordHigh;
    unsigned short FPGAState = statusWordLow & 0x000F;
    unsigned short FPGACfg = (statusWordLow & 0x0010) >> 4;
    unsigned short FPGAPLL = (statusWordLow & 0x0020) >> 5;

    std::ostringstream oss;
    oss << "TopLoopNum = " << topLoopNum << ", FPGA PLL = ";
    if (FPGAPLL == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    oss << ", FPGA Cfg = ";
    if (FPGACfg == 1)
    {
        oss << "true";
    }
    else
    {
        oss << "false";
    }
    oss << ", FPGAState = ";
    switch (FPGAState)
    {
    case 0:
        oss << "idle/none";
        break;
    case 1:
        oss << "transmit";
        break;
    case 2:
        oss << "receive";
        break;
    case 3:
        oss << "transcive";
        break;
    case 4:
        oss << "wait";
        break;
    case 5:
        oss << "rdwrram";
        break;
    case 6:
        oss << "program";
        break;
    default:
        oss << "invalid";
        break;
    }
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    return true;
}

//////////////////////////////////////////////////////////////////////////

ReadAuxInfoOpt::ReadAuxInfoOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_READ_AUX_INFO, 1, comHandle)
    , _replyEvent(onReply)
{

}

void ReadAuxInfoOpt::onExecute()
{
    logStart("[Read Aux Info]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 1;      // 读取单片机温度

    sendAndReceive();
}

void ReadAuxInfoOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool ReadAuxInfoOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Read Aux Info = Fail, Error Code = " + oss.str())));
        return false;
    }

    unsigned short auxInfo = _responseBuffer[RESPONSE_DATA_OFFSET];
    float temperature = auxInfo / 10.0f;

    std::ostringstream oss;
    oss << "Temperature = " << temperature;
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(oss.str())));

    return true;
}

//////////////////////////////////////////////////////////////////////////

EnterBSLModeOpt::EnterBSLModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply)
    : SerialPortOpt(INSB_ENTER_BSL_MODE, 1, comHandle)
    , _replyEvent(onReply)
{

}

void EnterBSLModeOpt::onExecute()
{
    logStart("[Enter BSL Mode]");

    // 装配指令
    _instructionBuffer[0] = (_instruction << 8) | (~_instruction & 0x00FF);
    _instructionBuffer[1] = _sendDataLen;
    _instructionBuffer[2] = 0x2828;

    sendAndReceive();
}

void EnterBSLModeOpt::onReply()
{
    if (_replyEvent)
    {
        _replyEvent();
    }
}

bool EnterBSLModeOpt::parseResponse()
{
    if (_errorCode != 0)
    {
        std::ostringstream oss;
        oss << _errorCode;
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Enter BSL Mode = Fail, Error Code = " + oss.str())));
        return false;
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Enter BSL Mode = Success")));
        return true;
    }
}