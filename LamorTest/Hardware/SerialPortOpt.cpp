#include "SerialPortOpt.h"

#include <sstream>
#include <iomanip>
#include <assert.h>

#include "../Actions/ActionRunner.h"
#include "../Actions/CommonAction.h"

const int maxInstructionLen = 512;

SerialPortOpt::SerialPortOpt(char instruction, int sendDataLen, SerialPortImpl* comHandle)
    : _instruction(instruction)
    , _sendDataLen(sendDataLen)
    , _instructionBuffer(nullptr)
    , _encodingBuffer(nullptr)
    , _responseBuffer(nullptr)
    , _responseDataLen(0)
    , _errorCode(0)
    , _comHandle(comHandle)
    , _outputToLog(true)
{
    allocateBuffer();

    _decodingBuffer = new EncodedData[maxInstructionLen];
    memset(_decodingBuffer, 0, sizeof(EncodedData) * maxInstructionLen);
}

SerialPortOpt::~SerialPortOpt()
{
    delete[] _instructionBuffer;
    _instructionBuffer = nullptr;
    delete[] _encodingBuffer;
    _encodingBuffer = nullptr;
    delete[] _decodingBuffer;
    _decodingBuffer = nullptr;

    if (_responseBuffer)
    {
        delete[] _responseBuffer;
        _responseBuffer = nullptr;
    }
}

void SerialPortOpt::allocateBuffer()
{
    _instructionLen = _sendDataLen + 3;
    assert(_instructionLen <= maxInstructionLen);

    if (_instructionBuffer)
    {
        delete[] _instructionBuffer;
    }
    _instructionBuffer = new unsigned short[_instructionLen];
    memset(_instructionBuffer, 0, sizeof(unsigned short) * _instructionLen);

    if (_encodingBuffer)
    {
        delete[] _encodingBuffer;
    }
    _encodingBuffer = new EncodedData[_instructionLen];
    memset(_encodingBuffer, 0, sizeof(EncodedData) * _instructionLen);
}

void SerialPortOpt::sendAndReceive()
{
    if (sendInstruction())
    {
        if (_responseBuffer)
        {
            delete[] _responseBuffer;
        }
        _responseBuffer = new unsigned short[_responseDataLen];
        memset(_responseBuffer, 0, sizeof(unsigned short) * _responseDataLen);

        // 解码
        decoding(_decodingBuffer, _responseDataLen, _responseBuffer);

        // 校验
        verifyResponse(_responseBuffer, _responseDataLen);

        // 向日志发送十六进制接收到的数据
        logResponse();
    }

    // 解析接收到的数据
    parseResponse();

    // 发送一条分隔符
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("----------------------------")));
}

bool SerialPortOpt::sendInstruction()
{
    // 生成指令校验和
    calcInstructionChecksum();

    // 向日志发送十六进制指令
    logInstruction();

    // 编码
    encoding(_instructionBuffer, _instructionLen, _encodingBuffer);

    unsigned long readByteCount;
    bool result = _comHandle->callSerial((const char*)_encodingBuffer, sizeof(EncodedData) * _instructionLen,
            (char*)_decodingBuffer, sizeof(EncodedData) * maxInstructionLen, readByteCount);
    if (result)
    {
        // 计算接收到的数据长度
        _responseDataLen = readByteCount / sizeof(EncodedData);
    }
    else
    {
        _errorCode = 0xFFFF;    // 超时
    }

    return result;
}

void SerialPortOpt::calcInstructionChecksum()
{
    unsigned short checksum = _sendDataLen;
    for (int i = 0; i < _sendDataLen; i++)
    {
        checksum ^= _instructionBuffer[i + INSTRUCTION_DATA_OFFSET];
    }
    _instructionBuffer[_instructionLen - 1] = checksum;
}

void SerialPortOpt::encoding(unsigned short* input, int length, EncodedData* output)
{
    assert(input);
    assert(length > 0 && length <= maxInstructionLen);
    assert(output);

    for (int i = 0; i < length; i++)
    {
        unsigned short inputData = input[i];

        output[i].data[0] = (inputData >> 8) & 0x1F;
        if (i == 0)
        {
            output[i].data[0] |= 0b01100000;   // StartF
        }
        else
        {
            output[i].data[0] |= 0b01000000;
        }
        output[i].data[0] &= 0b01111111;

        output[i].data[1] = (inputData & 0xE000) >> 13;
        output[i].data[1] |= (inputData & 0x00E0) >> 2;
        output[i].data[1] |= 0b10000000;
        output[i].data[1] &= 0b10111111;

        output[i].data[2] = inputData & 0x1F;
        if (i == length - 1)
        {
            output[i].data[2] |= 0b11100000;   // EndF
        }
        else
        {
            output[i].data[2] |= 0b11000000;
            output[i].data[2] &= 0b11011111;
        }

     //   printf("%.4X %.4X %.4X\n", output[i].data[0], output[i].data[1], output[i].data[2]);
    }
}

void SerialPortOpt::decoding(EncodedData* input, int length, unsigned short* output)
{
    assert(input);
    assert(length > 0 && length <= maxInstructionLen);
    assert(output);

    printf("收到数据总长度 %d 字\n", length);

    for (int i = 0; i < length; i++)
    {
        EncodedData inputData = input[i];

        char lowByte = inputData.data[2] & 0x1F;
        lowByte |= (inputData.data[1] & 0x38) << 2;

        char highByte = inputData.data[0] & 0x1F;
        highByte |= (inputData.data[1] & 0x07) << 5;

        output[i] = (lowByte & 0x00FF) | (highByte << 8);

        printf(" %d \t %.4X \t %c %c\n", output[i], output[i], lowByte, highByte);
    }
}

bool SerialPortOpt::verifyResponse(unsigned short* responseData, int length)
{
    // 检查数据长度是否符合规范
    unsigned short dataLength = _responseBuffer[0];
    if (dataLength + 2 != length)
    {
        _errorCode = 0x0003;        // data length error
        return false;
    }

    // 检查接收到的Instruction Word与发出时一致
    unsigned short instruction = _responseBuffer[1];
    if (instruction != _instructionBuffer[0])
    {
        _errorCode = 0x0001;        // INSB_CHK error
        return false;
    }

    // 校验Checksum
    unsigned short checksum = _responseBuffer[0];
    for (int i = 1; i < dataLength + 1; i++)
    {
        checksum ^= _responseBuffer[i];
    }
    if (checksum != _responseBuffer[dataLength + 1])
    {
        _errorCode = 0x0004;        // Checksum error
        return false;
    }

    // 检查ErrorCode
    _errorCode = _responseBuffer[2];
    if (_errorCode != 0)
        return false;

    return true;
}

std::string SerialPortOpt::extractString(unsigned short* buffer, int strLength)
{
    exchangeByte(buffer, strLength);

    char* strBuffer = new char[strLength * 2 + 1];
    memcpy(strBuffer, buffer, sizeof(char) * strLength * 2);
    strBuffer[strLength * 2] = '\0';

    std::string str = strBuffer;
    delete[] strBuffer;

    return str;
}

void SerialPortOpt::exchangeByte(unsigned short* buffer, int length)
{
    for (int i = 0; i < length; i++)
    {
        buffer[i] = (buffer[i] << 8) | (buffer[i] >> 8);
    }
}

void SerialPortOpt::logInstruction()
{
    if (!_outputToLog)
        return;

    std::string log = "Send = 0x";
    for (int i = 0; i < _instructionLen; i++)
    {
        std::ostringstream oss;
        oss << std::hex << std::uppercase << std::setfill('0') << std::setw(4) << _instructionBuffer[i];
        log += oss.str();
        log += " ";
    }
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(log)));
}

void SerialPortOpt::logResponse()
{
    if (!_outputToLog)
        return;

    std::string log = "Return = 0x";
    for (int i = 0; i < _responseDataLen; i++)
    {
        std::ostringstream oss;
        oss << std::hex << std::uppercase << std::setfill('0') << std::setw(4) << _responseBuffer[i];
        log += oss.str();
        log += " ";
    }
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(log)));

    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction(getDataTime() + " End")));
}