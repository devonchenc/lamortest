﻿#include "SerialPortImpl.h"

#include <iostream>
#include <thread>
#include <chrono>
#include <assert.h>

#if UsingCriticalSection
#define MLeaveCriticalSection(cs) LeaveCriticalSection(cs)
#define MEnterCriticalSection(cs) EnterCriticalSection(cs)
#define MInitializeCriticalSection(cs) InitializeCriticalSection(cs)
#define MDeleteCriticalSection(cs) DeleteCriticalSection(cs)
#else
#define MLeaveCriticalSection(cs) 
#define MEnterCriticalSection(cs) 
#define MInitializeCriticalSection(cs) 
#define MDeleteCriticalSection(cs)
#endif

SerialPortImpl::SerialPortImpl()
{
    _comHandle = (HANDLE)INVALID_HANDLE_VALUE;

    MInitializeCriticalSection(&_csCommunicationSync);
}

SerialPortImpl::~SerialPortImpl()
{
    closePort();
    MDeleteCriticalSection(&_csCommunicationSync);
}

#define PURGE_FLAG (PURGE_RXCLEAR | PURGE_TXCLEAR | PURGE_RXABORT | PURGE_TXABORT)

bool SerialPortImpl::initPort(const std::string& comPort, UINT baud /*= CBR_9600*/, char parity /*= 0*/,
    UINT databits /*= 8*/, UINT stopsbits /*= 1*/, COMMTIMEOUTS* commTimeouts)
{
    // 打开指定串口, 该函数内部已经有临界区保护, 上面请不要加保护
    if (!openPort(comPort))
        return false;

    // 进入临界段
    MEnterCriticalSection(&_csCommunicationSync);

    DCB dcb;
    memset(&dcb, 0, sizeof(DCB));
    dcb.DCBlength = sizeof(DCB);
    dcb.BaudRate = baud;
    dcb.ByteSize = databits;
    switch (parity)
    {
    case 0:
        dcb.Parity = NOPARITY;
        break;
    case 1:
        dcb.Parity = ODDPARITY;     // 奇校验
        break;
    case 2:
        dcb.Parity = EVENPARITY;    // 偶校验
        break;
    case 3:
        dcb.Parity = MARKPARITY;    // 标记校验
        break;
    }
    switch (stopsbits)
    {
    case 1:
        dcb.StopBits = ONESTOPBIT;
        break;
    case 2:
        dcb.StopBits = TWOSTOPBITS;
        break;
    case 3:
        dcb.StopBits = ONE5STOPBITS;
        break;
    }

    // 使用DCB参数配置串口状态
    BOOL isSuccess = SetCommState(_comHandle, &dcb);
    if (isSuccess && commTimeouts)
    {
        isSuccess = SetCommTimeouts(_comHandle, commTimeouts);
    }

    // 清空串口缓冲区
    PurgeComm(_comHandle, PURGE_FLAG);

    // 离开临界段
    MLeaveCriticalSection(&_csCommunicationSync);

    printf("Baud=%d parity=%d data=%d stop=%d, result:%d\n", baud, parity, databits, stopsbits, isSuccess);
    return isSuccess == TRUE;
}

UINT SerialPortImpl::getBytesInCOM()
{
    DWORD dwError = 0;
    // COMSTAT结构体,记录通信设备的状态信息
    COMSTAT comstat;
    memset(&comstat, 0, sizeof(COMSTAT));

    UINT bytesInQue = 0;
    // 在调用ReadFile和WriteFile之前,通过本函数清除以前遗留的错误标志
    if (ClearCommError(_comHandle, &dwError, &comstat))
    {
        // 获取在输入缓冲区中的字节数
        bytesInQue = comstat.cbInQue;
    }

    return bytesInQue;
}

bool SerialPortImpl::readChar(char &cRecved)
{
    if (_comHandle == INVALID_HANDLE_VALUE)
        return false;

    // 临界区保护
    MEnterCriticalSection(&_csCommunicationSync);

    // 从缓冲区读取一个字节的数据
    DWORD bytesRead = 0;
    BOOL bResult = ReadFile(_comHandle, &cRecved, 1, &bytesRead, NULL);
    if ((!bResult))
    {
        // 获取错误码
        DWORD dwError = GetLastError();
        printf("Reading error %d\n", dwError);
        // 清空串口缓冲区
        PurgeComm(_comHandle, PURGE_FLAG);
        MLeaveCriticalSection(&_csCommunicationSync);

        return false;
    }

    // 离开临界区
    MLeaveCriticalSection(&_csCommunicationSync);

    return (bytesRead == 1);
}

bool SerialPortImpl::readData(char* data, unsigned int length, DWORD& bytesRead)
{
    bytesRead = 0;

    if (_comHandle == INVALID_HANDLE_VALUE)
        return false;

    // 临界区保护
    MEnterCriticalSection(&_csCommunicationSync);

    // 从缓冲区读取数据 
    BOOL result = ReadFile(_comHandle, data, length, &bytesRead, NULL);
    if (!result)
    {
        // 获取错误码
        DWORD dwError = GetLastError();
        printf("reading error %d\n", dwError);
        // 清空串口缓冲区
        PurgeComm(_comHandle, PURGE_FLAG);
        MLeaveCriticalSection(&_csCommunicationSync);

        return false;
    }

    // 离开临界区
    MLeaveCriticalSection(&_csCommunicationSync);

    return (bytesRead == length);
}

bool SerialPortImpl::writeData(const char* pData, unsigned int length)
{
    DWORD bytesToSend = 0;

    if (_comHandle == INVALID_HANDLE_VALUE)
        return false;

    // 临界区保护
    MEnterCriticalSection(&_csCommunicationSync);

    // 向缓冲区写入指定量的数据
    BOOL result = WriteFile(_comHandle, pData, length, &bytesToSend, NULL);
    if (!result)
    {
        DWORD dwError = GetLastError();
        // 清空串口缓冲区
        PurgeComm(_comHandle, PURGE_FLAG);
        MLeaveCriticalSection(&_csCommunicationSync);

        return false;
    }
    //printf("send,%d :%s", BytesToSend, pData);
    // 离开临界区
    MLeaveCriticalSection(&_csCommunicationSync);

    return true;
}

#define TIME_DELAY      64
#define MAX_READ_TIMES  3

bool SerialPortImpl::callSerial(const char* inBuffer, unsigned int byteLength, 
    char* outBuffer, unsigned int outBufferSize, unsigned long& readByteCount)
{
    COMSTAT comStat;
    unsigned long errFlags = 0;
    ClearCommError(_comHandle, &errFlags, &comStat);
    PurgeComm(_comHandle, PURGE_FLAG);

    if (!writeData(inBuffer, byteLength))
        return false;

    readByteCount = 0;
    int readTimes = 0;

    do {
        std::this_thread::sleep_for(std::chrono::milliseconds(TIME_DELAY));
        readData(outBuffer, outBufferSize, readByteCount);
    } while (readByteCount == 0 && readTimes++ < MAX_READ_TIMES);

    if (readByteCount == 0)
    {
        printf("Read serial failed! Buffer:%s", inBuffer);
        return false;
    }
    return true;
}

bool SerialPortImpl::openPort(const std::string& comPort)
{
    // 进入临界段
    MEnterCriticalSection(&_csCommunicationSync);

    // 打开指定的串口
    _comHandle = CreateFileA(comPort.c_str(),    // 设备名,COM1,COM2等
        GENERIC_READ | GENERIC_WRITE,   // 访问模式,可同时读写
        0,                              // 共享模式,0表示不共享
        NULL,							// 安全性设置,一般使用NULL
        OPEN_EXISTING,					// 该参数表示设备必须存在,否则创建失败
        0,
        0);

    // 如果打开失败，释放资源并返回
    if (_comHandle == INVALID_HANDLE_VALUE)
    {
        MLeaveCriticalSection(&_csCommunicationSync);
        printf("Open com:%s error\n", comPort.c_str());
        return false;
    }
    printf("Open com:%s success\n", comPort.c_str());

    // 退出临界区
    MLeaveCriticalSection(&_csCommunicationSync);
    return true;
}

bool SerialPortImpl::isOpened() const
{
#ifdef _DEBUG
    return true;
#else
    return _comHandle != (HANDLE)INVALID_HANDLE_VALUE;
#endif
}

void SerialPortImpl::closePort()
{
    if (_comHandle != (HANDLE)INVALID_HANDLE_VALUE)
    {
        CloseHandle(_comHandle);
        _comHandle = (HANDLE)INVALID_HANDLE_VALUE;
    }
}