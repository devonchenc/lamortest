#pragma once

#include "Board.h"
#include "SerialPortImpl.h"

class DomesticBoard : public Board
{
public:
    DomesticBoard() {}
    virtual ~DomesticBoard() {}

    bool open() override;
    void close() override;

    bool isOpened() const { return _serialPort.isOpened(); }

    void update(int delta) override;

    void readDeviceInfo(InfoType infoType) override;

    void changeBaudrate(int baudrate) override;

    void comTest() override;

    void readStatus() override;

    void clearAllError() override;

    void syncTime() override;

    void readTime(bool compare = false) override;

    void writeFIRParameter(const std::vector<int>& value) override;

    void writeCICParameter(unsigned short value) override;

    void writeTimeCoreParameter(int address, const std::vector<TimeCore>& parameters) override;

    void readParameter() override;

    void startAcquire() override;

    void readAcquiredData() override;

    void writeRamData(uint64_t ramAddress, const std::vector<int>& data) override;

    void readRamData(uint64_t ramAddress, int readLen) override;

    void enterDVPMode() override;

    void exitDVPMode() override;

    void terminateExe() override;

    void readAuxInfo() override;

    void enterBSLMode() override;

protected:
    void onOpen() override {}
    void onClose() override {}

private:
    void printDeviceInfo(std::vector<std::string> vector);

    void compareTime(int readTime);

private:
    SerialPortImpl _serialPort;
};