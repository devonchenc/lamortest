#pragma once

#include <map>

#include "SerialPortOpt.h"

// 读取设备信息
class ReadDeviceInfoOpt : public SerialPortOpt
{
public:
    ReadDeviceInfoOpt(SerialPortImpl* comHandle, InfoType infoType, CallbackStringVector onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    InfoType _infoType;
    std::vector<std::string> _vector;

    CallbackStringVector _replyEvent;
};

// 改变设备波特率
class ChangeBaudrateOpt : public SerialPortOpt
{
public:
    ChangeBaudrateOpt(SerialPortImpl* comHandle, int baudrate, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    int _baudrate;

    CallbackVoid _replyEvent;
};

// 通讯测试
class ComTestOpt : public SerialPortOpt
{
public:
    ComTestOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 读取设备状态
class ReadStatusOpt : public SerialPortOpt
{
public:
    ReadStatusOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    unsigned short _statusWordHigh;
    unsigned short _statusWordLow;

    CallbackVoid _replyEvent;
};

// 清除设备错误状态
class ClearAllErrorOpt :public SerialPortOpt
{
public:
    ClearAllErrorOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;

private:
    CallbackVoid _replyEvent;
};

// 同步时间
class SyncTimeOpt : public SerialPortOpt
{
public:
    SyncTimeOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 读取设备时间
class ReadTimeOpt : public SerialPortOpt
{
public:
    ReadTimeOpt(SerialPortImpl* comHandle, CallbackInt onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    int _readTime;

    CallbackInt _replyEvent;
};

// 写FIR参数
class WriteFIRParameterOpt : public SerialPortOpt
{
public:
    WriteFIRParameterOpt(SerialPortImpl* comHandle, const std::vector<int>& value, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    std::vector<int> _value;

    CallbackVoid _replyEvent;
};

// 写CIC参数
class WriteCICParameterOpt : public SerialPortOpt
{
public:
    WriteCICParameterOpt(SerialPortImpl* comHandle, unsigned short value, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    unsigned short _value;

    CallbackVoid _replyEvent;
};

typedef std::map<std::string, std::string> TimeCore;

// 写TimeCore参数
class WriteTimeCoreParameterOpt : public SerialPortOpt
{
public:
    WriteTimeCoreParameterOpt(SerialPortImpl* comHandle, int address, const std::vector<TimeCore>& parameters, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    bool translatedintoCode(const TimeCore& timeCore, unsigned short* buffer);

private:
    int _address;
    std::vector<TimeCore> _parameters;

    CallbackVoid _replyEvent;
};

// 读参数
class ReadParameterOpt : public SerialPortOpt
{
public:
    ReadParameterOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 开始采集
class StartAcquireOpt : public SerialPortOpt
{
public:
    StartAcquireOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 读取采集完成的数据
class ReadAcquiredDataOpt : public SerialPortOpt
{
public:
    ReadAcquiredDataOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 写数据至设备RAM
class WriteRamDataOpt : public SerialPortOpt
{
public:
    WriteRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, const std::vector<int>& data, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    uint64_t _ramAddress;
    std::vector<int> _data;

    CallbackVoid _replyEvent;
};

// 从设备RAM读取数据
class ReadRamDataOpt : public SerialPortOpt
{
public:
    ReadRamDataOpt(SerialPortImpl* comHandle, uint64_t ramAddress, int readLen, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    uint64_t _ramAddress;
    int _readLen;

    CallbackVoid _replyEvent;
};

// 进入开发模式
class EnterDVPModeOpt : public SerialPortOpt
{
public:
    EnterDVPModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 退出开发模式
class ExitDVPModeOpt : public SerialPortOpt
{
public:
    ExitDVPModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 强制停止设备一切工作
class TerminateExeOpt : public SerialPortOpt
{
public:
    TerminateExeOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 读取设备辅助信息
class ReadAuxInfoOpt : public SerialPortOpt
{
public:
    ReadAuxInfoOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};

// 进入BSL状态
class EnterBSLModeOpt : public SerialPortOpt
{
public:
    EnterBSLModeOpt(SerialPortImpl* comHandle, CallbackVoid onReply);

protected:
    void onExecute() override;
    void onReply() override;
    bool parseResponse() override;

private:
    CallbackVoid _replyEvent;
};