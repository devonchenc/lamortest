﻿#include "DomesticBoard.h"

#include <functional>
#include <string>
#include <QSettings>
#include <QCoreApplication>
#include "../Hardware/DomesticBoardOpt.h"

#include "../Actions/ActionRunner.h"
#include "../Actions/CommonAction.h"

bool DomesticBoard::open()
{
    Board::open();

    QSettings settings(QCoreApplication::applicationDirPath() + "/config.ini", QSettings::IniFormat);
    std::string comPort = settings.value("COM/port", "COM1").toString().toStdString();
    int baudrate = settings.value("COM/baudrate", 1000000).toInt();
    
    COMMTIMEOUTS timeOut;
	timeOut.ReadIntervalTimeout = MAXDWORD;
	timeOut.ReadTotalTimeoutMultiplier = 0;
	timeOut.ReadTotalTimeoutConstant = 2000;
	timeOut.WriteTotalTimeoutMultiplier = 50;
	timeOut.WriteTotalTimeoutConstant = 2000;

    bool result = _serialPort.initPort(comPort, baudrate, 1, 8, 1, &timeOut);
    if (result)
    {
        onOpen();
    }
    else
    {
        printf("Init COM error.\n");

        _serialPort.closePort();
    }
    return result;
}

void DomesticBoard::close()
{
    Board::close();

    _serialPort.closePort();
}

void DomesticBoard::update(int delta)
{
    Board::update(delta);
}

void DomesticBoard::readDeviceInfo(InfoType infoType)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadDeviceInfoOpt(&_serialPort, infoType, std::bind(&DomesticBoard::printDeviceInfo, this, std::placeholders::_1))));
    }
}

void DomesticBoard::changeBaudrate(int baudrate)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ChangeBaudrateOpt(&_serialPort, baudrate, nullptr)));
    }
}

void DomesticBoard::comTest()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ComTestOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readStatus()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadStatusOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::clearAllError()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ClearAllErrorOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::syncTime()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new SyncTimeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readTime(bool compare)
{
    if (_serialPort.isOpened())
    {
        if (compare)
        {
            _scheduler.invoke(OperationPtr(new ReadTimeOpt(&_serialPort, std::bind(&DomesticBoard::compareTime, this, std::placeholders::_1))));
        }
        else
        {
            _scheduler.invoke(OperationPtr(new ReadTimeOpt(&_serialPort, nullptr)));
        }
    }
}

void DomesticBoard::writeFIRParameter(const std::vector<int>& value)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new WriteFIRParameterOpt(&_serialPort, value, nullptr)));
    }
}

void DomesticBoard::writeCICParameter(unsigned short value)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new WriteCICParameterOpt(&_serialPort, value, nullptr)));
    }
}

void DomesticBoard::writeTimeCoreParameter(int address, const std::vector<TimeCore>& parameters)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new WriteTimeCoreParameterOpt(&_serialPort, address, parameters, nullptr)));
    }
}

void DomesticBoard::readParameter()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadParameterOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::startAcquire()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new StartAcquireOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readAcquiredData()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadAcquiredDataOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::writeRamData(uint64_t ramAddress, const std::vector<int>& data)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new WriteRamDataOpt(&_serialPort, ramAddress, data, nullptr)));
    }
}

void DomesticBoard::readRamData(uint64_t ramAddress, int readLen)
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadRamDataOpt(&_serialPort, ramAddress, readLen, nullptr)));
    }
}

void DomesticBoard::enterDVPMode()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new EnterDVPModeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::exitDVPMode()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ExitDVPModeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::terminateExe()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new TerminateExeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::readAuxInfo()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new ReadAuxInfoOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::enterBSLMode()
{
    if (_serialPort.isOpened())
    {
        _scheduler.invoke(OperationPtr(new EnterBSLModeOpt(&_serialPort, nullptr)));
    }
}

void DomesticBoard::printDeviceInfo(std::vector<std::string> vector)
{
    for (int i = 0; i < vector.size(); i++)
    {
        std::cout << vector[i] << std::endl;
    }
}

void DomesticBoard::compareTime(int readTime)
{
    using namespace std::chrono;
    system_clock::time_point now = system_clock::now();
    // 获取1970.1.1以来的秒数
    seconds timeSinceEpoch = duration_cast<seconds>(now.time_since_epoch());
    int second = timeSinceEpoch.count();

    if (abs(readTime - second) > 1)
    {
        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("The device time does not match the system time")));
    }
    else
    {
        ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("The device time matches the system time")));
    }
}
