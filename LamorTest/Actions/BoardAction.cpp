﻿#include "BoardAction.h"

#include "../Hardware/Board.h"
#include "../Global.h"
#include "../Actions/ActionRunner.h"
#include "../Actions/CommonAction.h"

void OpenBoardAction::onExecute()
{
    if (BoardStation::instance().isOpened())
        return;

    if (BoardStation::instance().open())
    {
        BoardPtr board = BoardStation::instance().device();
        if (board)
        {
            ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("Connect = Success")));
        }
    }
    else
    {
        setState(eAS_Error);

        ActionRunner::instance().runAction(ActionPtr(new LogErrorAction("Connect = Fail")));
    }

    // 发送一条分隔符
    ActionRunner::instance().runAction(ActionPtr(new LogInfoAction("----------------------------")));
}

int OpenBoardAction::onError()
{
    _errorString = std::string("Open board error");

    return 1;
}

//////////////////////////////////////////////////////////////////////////

void CloseBoardAction::onExecute()
{
    if (!BoardStation::instance().isOpened())
        return;

    BoardStation::instance().close();
}

int CloseBoardAction::onError()
{
    _errorString = std::string("Close board error");

    return 2;
}

//////////////////////////////////////////////////////////////////////////

void ReadDeviceInfoAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->readDeviceInfo(_infoType);
    }
}

int ReadDeviceInfoAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ChangeBaudrateAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->changeBaudrate(_baudrate);
    }
}

int ChangeBaudrateAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ComTestAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->comTest();
    }
}

int ComTestAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ReadStatusAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->readStatus();
    }
}

int ReadStatusAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ClearAllErrorAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->clearAllError();
    }
}

int ClearAllErrorAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void SyncTimeAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->syncTime();
    }
}

int SyncTimeAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ReadTimeAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->readTime(_compare);
    }
}

int ReadTimeAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void WriteFIRParameterAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->writeFIRParameter(_value);
    }
}

int WriteFIRParameterAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void WriteCICParameterAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->writeCICParameter(_value);
    }
}

int WriteCICParameterAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void WriteTimeCoreParameterAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->writeTimeCoreParameter(_address, _parameters);
    }
}

int WriteTimeCoreParameterAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ReadParameterAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->readParameter();
    }
}

int ReadParameterAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void StartAcquireAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->startAcquire();
    }
}

int StartAcquireAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ReadAcquiredDataAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->readAcquiredData();
    }
}

int ReadAcquiredDataAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void WriteRamDataAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->writeRamData(_ramAddress, _data);
    }
}

int WriteRamDataAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ReadRamDataAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->readRamData(_ramAddress, _readLen);
    }
}

int ReadRamDataAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void EnterDVPModeAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->enterDVPMode();
    }
}

int EnterDVPModeAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ExitDVPModeAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->exitDVPMode();
    }
}

int ExitDVPModeAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void TerminateExeAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->terminateExe();
    }
}

int TerminateExeAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void ReadAuxInfoAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->readAuxInfo();
    }
}

int ReadAuxInfoAction::onError()
{
    return 0;
}

//////////////////////////////////////////////////////////////////////////

void EnterBSLModeAction::onExecute()
{
    BoardPtr board = BoardStation::instance().device();
    if (board && board->isOpened())
    {
        board->enterBSLMode();
    }
}

int EnterBSLModeAction::onError()
{
    return 0;
}