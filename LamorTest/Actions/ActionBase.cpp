﻿#include "ActionBase.h"

ActionBase::ActionBase()
    : _elapsedTime(0)
    , _neededTime(INT_MAX)
    , _state(eAS_Idle)
    , _ignoreTime(false)
{

}

void ActionBase::update(unsigned int delta)
{
    _elapsedTime += delta;

    if (isState(eAS_Idle))
    {
		setState(eAS_Run);
        onExecute();
    }

    if (isState(eAS_Run))
    {
        // 判断是否超时
        if (isTimeOut())
        {
            setState(_ignoreTime ? eAS_Finish : eAS_Error);
        }
        else if (onEstimate())
        {
            setState(eAS_Finish);
        }
    }
}