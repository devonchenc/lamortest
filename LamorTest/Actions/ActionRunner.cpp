﻿#include "ActionRunner.h"

#include <functional>
#include <assert.h>

ActionRunner& ActionRunner::instance()
{
    static ActionRunner instance;
    return instance;
}

void ActionRunner::runAction(ActionPtr action, bool ignoreTime, bool isPrior)
{
    assert(action);

    if (ignoreTime)
    {
        action->ignoreTime();
    }

    if (isPrior)
    {
        _workSequence.push_front(action);
    }
    else
    {
        _workSequence.push_back(action);
    }
}

void ActionRunner::update(unsigned int delta)
{
    if (!_workSequence.empty() && _activeAction == nullptr)
    {
        _activeAction = _workSequence.front();
        _workSequence.pop_front();
    }

    if (_activeAction)
    {
        _activeAction->update(delta);

        if (_activeAction == nullptr)
            return;

        if (_activeAction->isState(eAS_Finish))
        {
            _activeAction = nullptr;
        }
        else if (_activeAction->isState(eAS_Cancel))
        {
            if (_cancelHandler)
            {
                _cancelHandler(_activeAction->onCancle());
            }

            _cancelSequence.push_back(_activeAction);

            _activeAction = nullptr;
        }
        else if (_activeAction->isState(eAS_Error))
        {
            int errorCode = _activeAction->onError();
            if (_errorHandler)
            {
                _errorHandler(errorCode, _activeAction->errorString());
            }

            _errorSequence.push_back(_activeAction);

            // clear other actions
            _workSequence.clear();

            _activeAction = nullptr;
        }
    }

    if (_enableStatistics)
    {
        _elapsedTime += delta;
    }
}

bool ActionRunner::isEmpty() const
{
    return (_activeAction == nullptr) && _workSequence.empty();
}

int ActionRunner::count() const
{
    int size = int(_workSequence.size());
    return _activeAction ? size + 1 : size;
}

ActionRunner::ActionRunner()
    : _elapsedTime(0)
    , _remainTime(0)
    , _enableStatistics(false)
    , _cancelHandler(nullptr)
    , _errorHandler(nullptr)
{

}

ActionRunner::~ActionRunner()
{
    _workSequence.clear();
    _cancelSequence.clear();
    _errorSequence.clear();
    _activeAction = nullptr;
}

void ActionRunner::cancel()
{
    _workSequence.clear();

    if (_activeAction)
    {
        _activeAction->cancel();
    }
}

std::string ActionRunner::activeActionName()
{
    return _activeAction ? _activeAction->name() : "";
}
