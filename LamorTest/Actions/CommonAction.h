﻿#pragma once

#include "ActionBase.h"

class LogInfoAction : public ActionBase
{
public:
    LogInfoAction(const std::string& info) : _infoText(info) {}

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }

private:
    std::string _infoText;
};

class LogErrorAction : public ActionBase
{
public:
    LogErrorAction(const std::string& error) : _errorText(error) {}

protected:
    void onExecute() override;
    bool onEstimate() override { return true; }

private:
    std::string _errorText;
};