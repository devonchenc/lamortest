﻿#include "CommonAction.h"

#include "../Dumper.h"

void LogInfoAction::onExecute()
{
    Dumper::instance().info(_infoText);
}

void LogErrorAction::onExecute()
{
    Dumper::instance().error(_errorText);
}