﻿#include "Scheduler.h"

#include <iostream>
#include <assert.h>

Scheduler::Scheduler()
    : _sleepTime(20)
{

}

void Scheduler::keepAliveOpt(OperationPtr operation)
{
    assert(operation);
    _keepAliveOpt = operation;
}

void Scheduler::invoke(OperationPtr operation, bool isAsync)
{
    if (!isRunning())
        return;

    if (isAsync)
    {
        // 异步调用, 则插入队列
        _executeQueue.push(operation);
    }
    else
    {
        // 同步调用
        operation->execute();
        operation->reply();
    }
}

void Scheduler::start()
{
    _isRunning = true;
    _thread = std::thread(std::bind(&Scheduler::_run, this));
}

void Scheduler::_run()
{
    while (_isRunning || !_executeQueue.empty())
    {
        if (!_executeQueue.empty())
        {
            OperationPtr operation = _executeQueue.pop();
            operation->execute();
            _replyQueue.push(operation);

            std::this_thread::sleep_for(std::chrono::milliseconds(_sleepTime));
        }
        else
        {
            if (_keepAliveOpt)
            {
                _keepAliveOpt->execute();
            }
            std::this_thread::sleep_for(std::chrono::milliseconds(_sleepTime));
        }
    }
}

void Scheduler::stop()
{
    if (!_isRunning)
        return;

    _isRunning = false;

    // join后主程序会等待线程执行完毕
    _thread.join();
}

// 更新队列
void Scheduler::update()
{
    if (_isRunning || !_replyQueue.empty())
    {
		while (!_replyQueue.empty())
		{
			OperationPtr operation = _replyQueue.pop();
			if (operation)
            {
				operation->reply();

                if (operation->isLoop())
                {
                    _executeQueue.push(operation);
                }
			}
		}
    }
}
