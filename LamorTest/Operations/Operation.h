#pragma once

#include <vector>
#include <memory>
#include <functional>

class Operation
{
public:
    Operation();
    virtual ~Operation() {}

    void setDelayTime(int time) { _delayTime = time; }
    int delayTime() const { return _delayTime; }

    void execute();

    void reply();

    void setLoop(bool val) { _isLoop = val; }
    bool isLoop() const { return _isLoop; }

protected:
    void cancel() { _isCanceled = true; }

    virtual void onExecute() = 0;
    virtual void onReply() {};

private:
    int _delayTime;

    bool _isLoop;

    bool _isCanceled;
};

typedef std::shared_ptr<Operation> OperationPtr;
typedef std::function<void(void)> CallbackVoid;
typedef std::function<void(int)> CallbackInt;
typedef std::function<void(bool)> CallbackBool;
typedef std::function<void(float)> CallbackFloat;
typedef std::function<void(long, long, long)> CallbackLong;
typedef std::function<void(std::vector<std::string>)> CallbackStringVector;