#pragma once

#include <QMainWindow>
#include <QTimer>

QT_BEGIN_NAMESPACE
class QAction;
class QMenuBar;
class QToolBar;
QT_END_NAMESPACE

class InfoWidget;
class CommandWidget;

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget* parent = nullptr);
    ~MainWindow() {}

protected:
    void closeEvent(QCloseEvent* event) override;

private slots:
    void open();
    void save();
    void test();
    void setting();
    void quit();

    void onUpdateTimer();

private:
    void initUI();

    void createActions();

    void createToolbar();

    void initBoard();

private:
    QToolBar* _toolBar;

    QAction* _openAction;
    QAction* _saveAction;
    QAction* _testAction;
    QAction* _settingsAction;
    QAction* _exitAction;

    InfoWidget* _infoWidget;
    CommandWidget* _commandWidget;

    QTimer _updateTimer;
};
